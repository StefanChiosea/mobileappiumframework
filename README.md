# Project Setup


## Prerequisites
* Admin rights 
* Internet access 

# Android windows setup

### Step 1: Download and Install Java 8
### Step 2: Setup Java Environment Variables
### Setup JAVA_HOME  
	 - The location where is generally installed JDK is C:\Program Files\Java
	 - Copy the path to specific JDK E.g C:\Program Files\Java\jdk1.8.0_121
	 - Search in windows for "Edit the system environment variables"
	 - Click on "Environment variables" 
	 - In System variables create new entry titled JAVA_HOME and set the value to path mentioned above E.g C:\Program Files\Java\jdk1.8.0_121 and click ok.
	

### Setup JAVA path  
	 With JAVA_HOME set above, now can use the variable and set the java path 
	 - Search in windows for "Edit the system environment variables"
	 - Click on "Environment variables" 
	 - In System variables search for path entry 
	 - Click edit, and add %JAVA_HOME%\bin
	 
### Optional:  
	 Verify above setup was correct
	 - Open command prompt and type "where javac", this command should display C:\Program Files\Java\jdk1.8.0_201\bin\javadoc.exe
	 - Type "javac -version", this command should display java version and confirm that JAVA_HOME was set correctly E.g javac 1.8.0_201 which should match the jdk version you have used in **`JAVA_HOME`** environment variable.

### Step 3: Download and Install Android Studio
### Step 3.1: Install additional Android SDK tools
### Step 3.2: Setup Android Environment Variables 
### Setup ANDROID_HOME  
     - The default folder location where Android SDK is installed is C:\Users\Stefan\AppData\Local\Android\sdk
	 - Search in windows for "Edit the system environment variables"
	 - Click on "Environment variables"
	 - In System variables create new entry titled ANDROID_HOME and set the value to path mentioned above E.g C:\Users\Stefan\AppData\Local\Android\sdk and click ok.
### Setup Android path  
	 - Add 3 new entries to path under System variables for android
	 - first entry: platform-tools, under C:\Users\Stefan\AppData\Local\Android\sdk\platform-tools
	 - second entry: tools, under C:\Users\Stefan\AppData\Local\Android\sdk\tools
	 - third entry: bin, under C:\Users\Stefan\AppData\Local\Android\sdk\tools\bin
	 
### Optional:  
	 All 3 paths can be added as:
	 	%ANDROID_HOME%\platform-tools
		%ANDROID_HOME%\tool
		%ANDROID_HOME%\tools\bin
##
		
## iOS setup

***Note:
Below steps are for simulator only***

### Step 1: Download and Install latest version of Java 8
### Step 2: Download and Install Xcode https://developer.apple.com/download/
### Step 3: install Xcode Command Line Tools by typing in bash terminal **`xcode-select --install`** and accept the pop-up
### Step 4: Install Homebrew https://brew.sh/
### Step 5: Install Carthage by typing in terminal **`brew install carthage`**
## 
## Appium setup

### Appium desktop: 
### Step 1: Download and Install latest version of Appium https://github.com/appium/appium-desktop/releases
**This will be used for inspector both Android and iOS**
##
### Appium terminal WINDOWS: 
### Step 1: Download and Install npm https://nodejs.org/en/download/
### Step 2: Set environment path variable for **`C:\Program Files\nodejs\`**
### Step 3: Open terminal and type **`npm install -g appium`**

### Appium terminal MacOS: 
### Step 1: Open terminal and type **`brew install node`**
### Step 2: After above installation process finishes, type **`npm install -g appium`**



## The sample scenarios
One positive and one negative for login

```Gherkin
Feature: As a user I can login successfully or not depending on the validity of my credentials

  @ui @login
  Scenario: Entering valid credentials the user is able to login and see 'Dashboard' bar
    Given that the user's app is opened the login page
    When the user logs with valid credentials
    Then the user sees login error message


  @ui @login
  Scenario: Entering invalid credentials the user is not able to login and sees error message
    Given that the user's app is opened the login page
    When the user logs with invalid credentials
    Then the user sees login success message
```


The glue code for the positive scenario looks like this

```java
    @Given("that the user's app is opened the login page")
       public void open_app_at_login_page() {
           loginSteps.navigate_to_login_page();
       }

    @When("the user logs with valid credentials")
        public void log_in_user_with_valid_credentials() {
            loginSteps.login_with_username_and_password(TestConstant.VALID_USER_USERNAME, TestConstant.VALID_USER_PASSWORD);
       }

    @Then("the user sees login success message")
        public void assert_login_success_message() {
            loginSteps.assert_login_message(TestConstant.MESSAGE_SUCCESS_LOGIN);
        }
```