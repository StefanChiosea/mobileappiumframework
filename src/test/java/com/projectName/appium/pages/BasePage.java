package com.projectName.appium.pages;


import com.projectName.appium.listeners.ExtentTestListener;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;


public class BasePage extends ExtentTestListener {


    public static String deviceRunOption = System.getProperty("deviceRunOption");
    protected final AppiumDriver driver;

    // private variables

    public BasePage(AppiumDriver appiumDriver) {
        this.driver = appiumDriver;
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver, Duration.ofSeconds(5)), this);
    }
}


