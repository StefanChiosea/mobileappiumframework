package com.projectName.appium.stepDefinitions;

import com.projectName.appium.listeners.ExtentTestListener;
import com.projectName.appium.pages.BasePage;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseSteps extends ExtentTestListener {

    BasePage basePage;

    @BeforeSuite
    public void statAppiumServer() {
        startAppium();
    }

    @Before
    public void setUp() {
        instantiateDriverObject();
        basePage = new BasePage(getDriver());
    }

    /*@After
    public void closeDriver() {
        closeDriverObjects();
    }*/

    @AfterSuite
    public void closeAppium() {
        closeDriverObjects();
        closeAppiumServer();
    }


    @When("^navigates to \"([^\"]*)\"$")
    public void navigatesTo(String page) {

    }


}
