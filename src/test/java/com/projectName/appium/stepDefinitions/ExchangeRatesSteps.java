package com.projectName.appium.stepDefinitions;

import com.projectName.appium.listeners.ExtentTestListener;
import com.projectName.appium.pages.exchange_rates.ExRateConverterPage;
import cucumber.api.java.en.And;

public class ExchangeRatesSteps extends ExtentTestListener {

    private ExRateConverterPage exRateConverterPage = new ExRateConverterPage(getDriver());

    @And("^assert exchange rates page title$")
    public void assertPageTitle() {
        testPass("Assert exchange rate tab label");
    }
}
