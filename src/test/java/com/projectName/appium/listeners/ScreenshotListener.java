package com.projectName.appium.listeners;

import com.projectName.appium.utils.Log4Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.projectName.appium.AppiumBase.getDriver;
import static com.projectName.appium.listeners.ExtentTestListener.testError;

public class ScreenshotListener extends TestListenerAdapter {

    private boolean createFile(File screenshot) {
        boolean fileCreated = false;

        if (screenshot.exists()) {
            fileCreated = true;
        } else {
            File parentDirectory = new File(screenshot.getParent());
            if (parentDirectory.exists() || parentDirectory.mkdirs()) {
                try {
                    fileCreated = screenshot.createNewFile();
                } catch (IOException errorCreatingScreenshot) {
                    Log4Test.trace(" " + errorCreatingScreenshot);
                }
            }
        }

        return fileCreated;
    }

    private void writeScreenshotToFile(WebDriver driver, File screenshot) {
        try {
            FileOutputStream screenshotStream = new FileOutputStream(screenshot);
            screenshotStream.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
            screenshotStream.close();
        } catch (IOException unableToWriteScreenshot) {
            Log4Test.error("Unable to write " + screenshot.getAbsolutePath());
            Log4Test.trace(" " + unableToWriteScreenshot);
        }
    }

    @Override
    public void onTestFailure(ITestResult failingTest) {
        try {
            WebDriver driver = getDriver();
            String screenshotDirectory = System.getProperty("screenshotDirectory", "target/screenshots");
            String screenshotAbsolutePath = screenshotDirectory + File.separator + System.currentTimeMillis() + "_" + failingTest.getName() + ".png";
            File screenshot = new File(screenshotAbsolutePath);
            if (createFile(screenshot)) {
                try {
                    writeScreenshotToFile(driver, screenshot);
                } catch (ClassCastException weNeedToAugmentOurDriverObject) {
                    Log4Test.trace("" + weNeedToAugmentOurDriverObject);
                    writeScreenshotToFile(new Augmenter().augment(driver), screenshot);
                }
                Log4Test.info("Written screenshot to " + screenshotAbsolutePath);
            } else {
                Log4Test.error("Unable to create " + screenshotAbsolutePath);
            }
        } catch (Exception ex) {
            Log4Test.error("Unable to capture screenshot...");
            testError(String.valueOf(ex));
        }
    }
}