package com.projectName.appium.listeners;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.projectName.appium.AppiumBase;
import com.projectName.appium.utils.CaptureScreenShot;
import com.projectName.appium.utils.ExtentManager;
import com.projectName.appium.utils.Log4Test;
import org.testng.*;

import java.io.IOException;

import static org.testng.Assert.fail;


public class ExtentTestListener extends AppiumBase implements ITestListener {

    protected static final ThreadLocal<ExtentTest> test = new ThreadLocal<>();
    //Extent Report Declarations
    private static final ExtentReports extent = ExtentManager.createInstance();

    /*public static void testInfo(String infoMessage) {
        Log4Test.info(infoMessage);
        test.get().info(infoMessage);
    }

    public static void testError(String errorMessage) {
        Log4Test.info(errorMessage);
        test.get().error(errorMessage);

    }*/

      /*public void takePicture() throws Exception{
           String screenShot = CaptureScreenShot.captureScreen(getDriver(), CaptureScreenShot.generateFileName(result));
           test.get().addScreenCaptureFromPath(screenShot);
       }*/
    /*protected static void testPass(String assertMessage) {
        Log4Test.test(assertMessage);
        test.get().pass(assertMessage);
    }*/

    public static void testPass(String assertMessage) {
        Log4Test.pass(assertMessage);
        try {
            test.get().pass(assertMessage);
        } catch (NullPointerException npe) {
            Log4Test.trace(npe + " - TestNG not initialised");
        }
    }

    public static void testDebugMessage(String testDebugMessage) {
        Log4Test.debug(testDebugMessage);
        try {
            test.get().debug(testDebugMessage);
        } catch (NullPointerException npe) {
            Log4Test.trace(npe + " - TestNG not initliased");
        }
    }

    public static void testInfo(String testInfoMessage) {
        Log4Test.info(testInfoMessage);
        try {
            test.get().info(testInfoMessage);
        } catch (NullPointerException npe) {
            Log4Test.trace(npe + " - TestNG not initliased");
        }
    }

    public static void testTestMessage(String testTestMessage) {
        Log4Test.test(testTestMessage);
        try {
            test.get().info(testTestMessage);
        } catch (NullPointerException npe) {
            Log4Test.trace(npe + " - TestNG not initliased");
        }
    }

    public static void testWarnMessage(String testWarnMessage) {
        Log4Test.warn(testWarnMessage);
        try {
            test.get().warning(testWarnMessage);
        } catch (NullPointerException npe) {
            Log4Test.trace(npe + " - TestNG not initliased");
        }
    }

    public static void testError(String testErrorMessage) {
        Log4Test.error(testErrorMessage);
        try {
            test.get().error(testErrorMessage);
        } catch (NullPointerException npe) {
            Log4Test.trace(npe + " - TestNG not initliased");
        }
    }

    public static void testFailMessage(String testFailMessage) {
        //Log4Test.fatal(testFailMessage);
        //test.get().fail(testFailMessage);
        fail(testFailMessage);
    }

    public static void testSkipMessage(String testSkipMessage) {
        Log4Test.warn(testSkipMessage);
        try {
            test.get().skip(testSkipMessage);
        } catch (NullPointerException npe) {
            Log4Test.trace(npe + " - TestNG not initliased");
        }
        throw new SkipException(testSkipMessage);
    }

    @Override
    public synchronized void onStart(ITestContext context) {
        Log4Test.info("Extent Reports Version 3 Test Suite started!");
    }

    @Override
    public synchronized void onFinish(ITestContext context) {
        Log4Test.info(("Extent Reports Version 3  Test Suite is ending!"));
        extent.flush();
    }

    @Override
    public synchronized void onTestStart(ITestResult result) {
        Log4Test.test((result.getMethod().getMethodName() + " started!"));
        ExtentTest extentTest = extent.createTest(getTestName(), result.getMethod().getDescription());
        test.set(extentTest);
        for (String group : result.getMethod().getGroups()) {
            test.get().assignCategory(group);
        }
    }

    @Override
    public synchronized void onTestSuccess(ITestResult result) {
        Log4Test.pass((result.getMethod().getMethodName() + " passed!"));
        test.get().pass("Test passed");
    }

    @Override
    public synchronized void onTestFailure(ITestResult result) {
        Log4Test.error((result.getMethod().getMethodName() + " failed!"));

        String screenShot = CaptureScreenShot.captureScreen(getDriver(), CaptureScreenShot.generateFileName(result));

        try {
            test.get().fail(result.getThrowable().toString());// + "" + test.get().addScreenCaptureFromPath(screenShot));
            test.get().fail("Screen Shot : " + test.get().addScreenCaptureFromPath(screenShot));
        } catch (IOException e) {
            testError(String.valueOf(e));
            Assert.fail();
        }

        extent.flush();
    }

    @Override
    public synchronized void onTestSkipped(ITestResult result) {
        Log4Test.warn((result.getMethod().getMethodName() + " skipped!"));
        test.get().skip(result.getThrowable());
    }

    //@Override
    public synchronized void onTestIgnored(ITestResult result) {
        Log4Test.warn((result.getMethod().getMethodName() + " ignored!"));
        test.get().skip(result.getThrowable());
        result.setStatus(ITestResult.FAILURE);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        Log4Test.warn(("onTestFailedButWithinSuccessPercentage for " + result.getMethod().getMethodName()));
    }
}