package com.projectName.appium.listeners;

import com.epam.reportportal.testng.BaseTestNGListener;
import com.epam.reportportal.testng.ITestNGService;
import com.epam.reportportal.testng.TestNGService;
import com.projectName.appium.utils.Log4Test;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;
import rp.com.google.common.base.Supplier;
import rp.com.google.common.base.Suppliers;

import java.util.Properties;

public class BaseTestNGListenerWithParameter extends BaseTestNGListener {
    private static final Supplier<ITestNGService> SERVICE = Suppliers.memoize(() -> {
        Properties props = new Properties();
        boolean reportPortalStatus = Boolean.parseBoolean(System.getProperty("reportPortalActivate"));
        Log4Test.debug("ReportPortal status: " + reportPortalStatus);
        if (reportPortalStatus) {
            return new TestNGService();
        } else {
            return new ITestNGService() {
                @Override
                public void startLaunch() {

                }

                @Override
                public void finishLaunch() {

                }

                @Override
                public void startTestSuite(ISuite iSuite) {

                }

                @Override
                public void finishTestSuite(ISuite iSuite) {

                }

                @Override
                public void startTest(ITestContext iTestContext) {

                }

                @Override
                public void finishTest(ITestContext iTestContext) {

                }

                @Override
                public void startTestMethod(ITestResult iTestResult) {

                }

                @Override
                public void finishTestMethod(String s, ITestResult iTestResult) {

                }

                @Override
                public void startConfiguration(ITestResult iTestResult) {

                }

                @Override
                public void sendReportPortalMsg(ITestResult iTestResult) {

                }
            };
        }
    });

    public BaseTestNGListenerWithParameter() {
        super(SERVICE.get());
    }

}
