package com.projectName.appium.runners;

import com.cucumber.listener.Reporter;
import com.projectName.appium.listeners.AbstractTestNGCucumberTestsCustom;
import cucumber.api.CucumberOptions;
import org.junit.AfterClass;

import java.io.File;

@CucumberOptions(
        features = "src/test/resources/features/Contact.feature",
        glue = {"com.athena.appium.stepDefinitions"}
        //plugin = {"com.cucumber.listener.ExtentCucumberFormatter:TestReport/CucumberExtentReport.html"}
)
public class ContactPageRunner extends AbstractTestNGCucumberTestsCustom {
    @AfterClass
    public static void writeExtentReport() {
        Reporter.loadXMLConfig(new File(System.getProperty("reportConfigPath")));
    }
}
