package com.projectName.appium.config;

import com.projectName.appium.utils.Log4Test;
import com.projectName.appium.utils.testdata.TestData;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.*;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;

public enum AppiumDriverType implements AppiumDriverSetup {

    ANDROID {
        public DesiredCapabilities getDesiredCapabilities(String pathToAppFile) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, System.getProperty("deviceName"));
            capabilities.setCapability(MobileCapabilityType.UDID, System.getProperty("deviceUDID"));
            capabilities.setCapability(AndroidMobileCapabilityType.AVD, System.getProperty("avd"));
            if (Boolean.parseBoolean(System.getProperty("registerDeviceLoginBoolean"))) {
                capabilities.setCapability(MobileCapabilityType.NO_RESET, true);

                Log4Test.warn("this works for register device, it doesn't remove the reg device, but creates problems at closing and starting other tests");
                Log4Test.warn("capabilities.setCapability(MobileCapabilityType.NO_RESET, true)");

                capabilities.setCapability("dontStopAppOnReset", false);

                Log4Test.warn("this seems to fix it .. to be monitored");
                Log4Test.warn("capabilities.setCapability(\"dontStopAppOnReset\", false)");
            } else {
                capabilities.setCapability("dontStopAppOnReset", true);
                capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
            }
            capabilities.setCapability("adbExecTimeout", 60000);
            if (System.getProperty("adbPort") != null) {
                capabilities.setCapability(AndroidMobileCapabilityType.ADB_PORT, Integer.valueOf(System.getProperty("adbPort")));
            }
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, System.getProperty("platformVersion"));
            capabilities.setCapability(MobileCapabilityType.APP, SetPackageByEnv.setPathToAppVariable(pathToAppFile));
            capabilities.setCapability("appPackage", TestData.APP_PACKAGE);
            capabilities.setCapability("appActivity", TestData.APP_ACTIVITY);
            capabilities.setCapability("uiautomator2ServerInstallTimeout", 60000);
            capabilities.setCapability("uiautomator2ServerLaunchTimeout", 60000);
            capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 1200);
            if (System.getProperty("systemPort") != null) {
                capabilities.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, Integer.valueOf(System.getProperty("systemPort")));
            } else {
                capabilities.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, System.getProperty("systemPort"));
            }
            return capabilities;
        }

        public AppiumDriver getWebDriverObject(URL appiumServerURL, DesiredCapabilities capabilities) {
            return new AndroidDriver<>(appiumServerURL, capabilities);
        }
    },
    IOS {
        public DesiredCapabilities getDesiredCapabilities(String pathToAppFile) {


            DesiredCapabilities capabilities = DesiredCapabilities.iphone();
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, System.getProperty("deviceName"));
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, System.getProperty("platformVersion"));
            capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
            capabilities.setCapability(MobileCapabilityType.UDID, System.getProperty("deviceUDID"));
            capabilities.setCapability(MobileCapabilityType.APP, SetPackageByEnv.setPathToAppVariable(pathToAppFile));
            capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
            capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, TestData.BUNDLE_ID);
            capabilities.setCapability(IOSMobileCapabilityType.SHOW_XCODE_LOG, false);
            capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 1200);
            if (System.getProperty("wdaLocalPort") != null) {
                capabilities.setCapability(IOSMobileCapabilityType.WDA_LOCAL_PORT, Integer.valueOf(System.getProperty("wdaLocalPort")));
            } else {
                capabilities.setCapability(IOSMobileCapabilityType.WDA_LOCAL_PORT, System.getProperty("wdaLocalPort"));
            }
            return capabilities;
        }

        public AppiumDriver getWebDriverObject(URL appiumServerURL, DesiredCapabilities capabilities) {
            return new IOSDriver<>(appiumServerURL, capabilities);
        }
    },
}