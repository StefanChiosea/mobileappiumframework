package com.projectName.appium.config;

import com.projectName.appium.utils.Log4Test;
import com.projectName.appium.utils.SetDevicesProps;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;

import static com.projectName.appium.listeners.ExtentTestListener.testError;

public class AppiumFactory extends SetDevicesProps {

    private final AppiumDriverType defaultDriverType = setDriverType();
    private final String appiumConfig = System.getProperty("driverType").toUpperCase();
    private final boolean useRemoteWebDriver = Boolean.getBoolean("remoteDriver");
    private final String pathToAppFile = System.getProperty("pathToAppFile"); //  // pathToAppFile
    private final String appiumServerLocation = System.getProperty("appiumServerLocation", "http://127.0.0.1:4723/wd/hub");
    private AppiumDriver driver;
    private AppiumDriverType selectedDriverType;

    public static AppiumDriverType setDriverType() {
        AppiumDriverType value;
        String driverTypeString = System.getProperty("driverType");
        switch (driverTypeString) {
            case "ANDROID":
                value = AppiumDriverType.ANDROID;
                break;
            case "IOS":
                value = AppiumDriverType.IOS;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + System.getProperty("driverType"));
        }
        return value;
    }

    public void quitDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    private void determineEffectiveDriverType() {
        AppiumDriverType driverType = defaultDriverType;
        try {
            driverType = AppiumDriverType.valueOf(appiumConfig);
        } catch (IllegalArgumentException ie) {
            Log4Test.trace("" + ie);
            Log4Test.error("Unknown driver specified, defaulting to '" + driverType + "'...");
        } catch (NullPointerException npe) {
            Log4Test.trace("" + npe);
            Log4Test.error("No driver specified, defaulting to '" + driverType + "'...");
        }
        selectedDriverType = driverType;
    }

    private void instantiateWebDriver(DesiredCapabilities desiredCapabilities) {
        Log4Test.info("\n");
        Log4Test.info("Current Appium Config Selection: " + selectedDriverType);
        Log4Test.info("Current Appium Server Location: " + appiumServerLocation);
        Log4Test.info("\n");

        if (useRemoteWebDriver) {
            URL seleniumGridURL = null;
            try {
                seleniumGridURL = new URL(System.getProperty("appiumServerURL"));
            } catch (MalformedURLException e) {
                testError(String.valueOf(e));
                Assert.fail();
            }

            String desiredVersion = System.getProperty("desiredVersion");
            String desiredPlatform = System.getProperty("desiredPlatform");

            if (null != desiredPlatform && !desiredPlatform.isEmpty()) {
                desiredCapabilities.setPlatform(Platform.valueOf(desiredPlatform.toUpperCase()));
            }

            if (null != desiredVersion && !desiredVersion.isEmpty()) {
                desiredCapabilities.setVersion(desiredVersion);
            }

            driver = (AppiumDriver) new RemoteWebDriver(seleniumGridURL, desiredCapabilities);
        } else {
            try {
                driver = selectedDriverType.getWebDriverObject(new URL(appiumServerLocation), desiredCapabilities);
            } catch (MalformedURLException e) {
                testError(String.valueOf(e));
                Assert.fail();
            }
        }
    }

    public AppiumDriver getDriver() {
        if (driver == null) {
            determineEffectiveDriverType();
            DesiredCapabilities desiredCapabilities = selectedDriverType.getDesiredCapabilities(pathToAppFile);
            instantiateWebDriver(desiredCapabilities);
        }

        return driver;
    }

}

