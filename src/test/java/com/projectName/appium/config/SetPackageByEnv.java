package com.projectName.appium.config;

import com.projectName.appium.utils.Log4Test;

public class SetPackageByEnv {

    public static String setPathToAppVariable(String pathToAppFile) {

        String valueVariable;
        String appPath = "defaultValue";
        String valuePath = System.getProperty("appEnv");
        String pathToAppConfig = System.getProperty("pathToAppVariable");


        if (pathToAppConfig.equalsIgnoreCase("local")) {
            Log4Test.trace("App install is from local");
            valueVariable = pathToAppFile;
        } else {
            Log4Test.trace("App install is from jenkins");
            valueVariable = appPath;
        }

        return valueVariable;
    }
}
