package com.projectName.appium.utils;

import com.projectName.appium.config.AppiumFactory;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.remote.DesiredCapabilities;

public class AppiumServerJava extends AppiumFactory {

    private static AppiumDriverLocalService service;
    private static AppiumServiceBuilder builder;
    private static DesiredCapabilities cap;
    private static int serverPort;

    public static void startServer() {
        String serverFullUrl = System.getProperty("appiumServerLocation");
        String serverIp = serverFullUrl.split(":")[1].replace("//", "");
        //serverPort = Integer.parseInt(serverFullUrl.split(":")[2].replace("/wd/hub", ""));
        Log4Test.debug("serverIp " + serverIp);

        //Build the Appium service
        builder = new AppiumServiceBuilder();
        builder.withIPAddress(serverIp);
        //  builder.usingPort(serverPort);
        builder.usingAnyFreePort();
        builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");


        //Start the server with the builder
        service = AppiumDriverLocalService.buildService(builder);
        service.start();
        serverFullUrl = service.getUrl().toString();
        Log4Test.debug("Appium started on: " + serverFullUrl);
        System.setProperty("appiumServerLocation", serverFullUrl);
        serverPort = Integer.parseInt(serverFullUrl.split(":")[2].replace("/wd/hub", ""));
        Log4Test.debug("serverPort " + serverPort);
    }

    public static void stopServer() {
        Log4Test.debug("Stopping server: " + service.getUrl());
        service.stop();
    }
}