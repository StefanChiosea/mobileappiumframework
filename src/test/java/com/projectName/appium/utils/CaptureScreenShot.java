package com.projectName.appium.utils;

import com.projectName.appium.listeners.ExtentTestListener;
import io.appium.java_client.MobileDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.projectName.appium.utils.Log4Test.uploadSS;

public class CaptureScreenShot extends ExtentManager {
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd SSS");
    private static String WINDOWS_SS_DIR = "defaultValue";
    private static String MAC_SS_DIR = "defaultValue";
    private static String WINDOWS_SS_DIR_RELATIVE = "defaultValue";
    private static String MAC_SS_DIR_RELATIVE = "defaultValue";

    public static String captureScreen(MobileDriver driver, String screenName) {

        TakesScreenshot screen = (TakesScreenshot) driver;
        File src = screen.getScreenshotAs(OutputType.FILE);
        WINDOWS_SS_DIR = System.getProperty("user.dir") + "\\Test-ScreenShots\\" + "Automated-Test-Report-" + setTimeStampReport() + "_" + System.getProperty("deviceRunOption") + "\\" + screenName + ".png";
        MAC_SS_DIR = System.getProperty("user.dir") + "/Test-ScreenShots/" + "Automated-Test-Report-" + setTimeStampReport() + "_" + System.getProperty("deviceRunOption") + "/" + screenName + ".png";

        String dest = getSSDirByPlatform(getCurrentPlatform());

        File target = new File(dest);
        try {
            FileUtils.copyFile(src, target);
        } catch (IOException e) {
            ExtentTestListener.testError(String.valueOf(e));
            Assert.fail("error wile copy screneshot");
        }
        uploadSS(target, dest);

        return dest;
    }


    public static String generateFileName(ITestResult result) {
        Date date = new Date();
        return result.getName() + "_" + dateFormat.format(date);

    }

    //Select the extent report file location based on platform
    private static String getSSDirByPlatform(Platform platform) {
        String ssDir = "defaultValue";
        switch (platform) {
            case MAC:
                ssDir = MAC_SS_DIR;
                Log4Test.info("SS dir for MAC: " + ssDir + "\n");
                break;
            case WINDOWS:
                ssDir = WINDOWS_SS_DIR;
                Log4Test.trace("SS dir for WINDOWS: " + ssDir + "\n");
                break;
            default:
                Log4Test.trace("SS dir has not been set! There is a problem!\n");
                break;
        }
        return ssDir;
    }
}