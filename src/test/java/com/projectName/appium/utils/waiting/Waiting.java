package com.projectName.appium.utils.waiting;

import com.projectName.appium.listeners.ExtentTestListener;
import com.projectName.appium.utils.Log4Test;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static com.projectName.appium.utils.commonMethods.CommonTask.checkMobileElementListSizeIsNotEmpty;

public class Waiting extends ExtentTestListener {

    // --------------------------------------- Explicit Waits

    public static void elementToBeClickable(MobileDriver driver, MobileElement element, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to be clickable");
            new WebDriverWait(driver, 7).until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException | UnreachableBrowserException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element : " + elementName + " is not clickable within : " + 7 + " seconds");
        } catch (NoSuchElementException nee) {
            Log4Test.trace("" + nee);
            testFailMessage("Element not found : " + elementName);
        }

    }

    public static void elementToBeClickable(MobileDriver driver, List<MobileElement> element, int index, int seconds, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to be clickable");
            checkMobileElementListSizeIsNotEmpty(element, index, seconds);
            new WebDriverWait(driver, seconds).until(ExpectedConditions.elementToBeClickable(element.get(index)));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element index : " + index + " with element " + elementName + " is not clickable within : " + seconds + " seconds");
        } catch (IndexOutOfBoundsException ie) {
            Log4Test.trace("" + ie);
            testFailMessage("Element not found : " + elementName);
        }
    }

    public static void elementToBeClickable(MobileDriver driver, MobileElement element, int seconds, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to be clickable");
            new WebDriverWait(driver, seconds).until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element : " + elementName + " is not clickable within : " + seconds + " seconds");
        } catch (NoSuchElementException nee) {
            Log4Test.trace("" + nee);
            testFailMessage("Element not found : " + elementName);
        }

    }

    public static void tryElementToBeClickable(MobileDriver driver, MobileElement element, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to be clickable");
            new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testInfo("Element : " + elementName + " is not clickable within : " + 30 + " seconds");
        } catch (NoSuchElementException nee) {
            Log4Test.trace("" + nee);
            testInfo("Element not found : " + elementName);
        }
    }

    public static void tryElementToBeClickable(MobileDriver driver, MobileElement element, int seconds, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to be clickable");
            new WebDriverWait(driver, seconds).until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testInfo("Element : " + elementName + " is not clickable within : " + seconds + " seconds");
        } catch (NoSuchElementException nee) {
            Log4Test.trace("" + nee);
            testInfo("Element not found : " + elementName);
        }
    }

    public static void textToBePresentInElement(MobileDriver driver, MobileElement element, String text, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for text : " + text + " : to be present in element");
            new WebDriverWait(driver, 60).until(ExpectedConditions.textToBePresentInElement(element, text));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Text : " + text + " is not present in element within : " + 60 + " seconds");
        }
    }

    public static void textToBePresentInElement(MobileDriver driver, MobileElement element, String text, String elementName, int seconds) {
        try {
            testInfo("> " + elementName + " - waiting for text : " + text + " : to be present in element");
            new WebDriverWait(driver, seconds).until(ExpectedConditions.textToBePresentInElement(element, text));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Text : " + text + " is not present in element within : " + seconds + " seconds");
        }
    }

    public static void textNotToBePresentInElement(MobileDriver driver, MobileElement element, String text, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for text : " + text + " : to not be present in element");
            new WebDriverWait(driver, 60).until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(element, text)));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Text : " + text + " is present in element within : " + 60 + " seconds");
        }
    }


    public static void textToBePresentInElementByTime(MobileDriver driver, MobileElement element, int seconds, String text, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for text : " + text + " : to be present in element");
            new WebDriverWait(driver, seconds).until(ExpectedConditions.textToBePresentInElement(element, text));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Text : " + text + " is not present in element within : " + seconds + " seconds");
        }
    }

    public static void visibilityOfElement(MobileDriver driver, MobileElement element, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to be visible");
            new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(element));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element : " + elementName + " is not visible within : " + 60 + " seconds it failed with TimeoutException");
        } catch (NoSuchElementException nspe) {
            Log4Test.trace("" + nspe);
            testFailMessage("Element : " + elementName + " is not visible within : " + 60 + " seconds it failed with NoSuchElementException");
        } catch (StaleElementReferenceException sere) {
            long startTime = System.currentTimeMillis();
            Log4Test.trace("" + sere);
            boolean elementIsVisible = false;
            while (!elementIsVisible && (System.currentTimeMillis() - startTime) < (60 * 1000)) {
                try {
                    new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(element));
                    elementIsVisible = element.isDisplayed();
                } catch (StaleElementReferenceException sere2) {
                    Log4Test.trace("" + sere2);
                }
            }
            if (!elementIsVisible) {
                testFailMessage("Element : " + elementName + " is not visible within : " + 60 + " seconds. It failed with StaleElementReferenceException");
            }
        }
    }

    public static void visibilityOfElement(MobileDriver driver, MobileElement element, int seconds, String
            elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to be visible");
            new WebDriverWait(driver, seconds).until(ExpectedConditions.visibilityOf(element));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element : " + elementName + " is not visible within : " + seconds + " seconds it failed with TimeoutException");
        } catch (NoSuchElementException nspe) {
            Log4Test.trace("" + nspe);
            testFailMessage("Element : " + elementName + " is not visible within : " + seconds + " seconds it failed with NoSuchElementException");
        } catch (StaleElementReferenceException sere) {
            Log4Test.trace("" + sere);
            long startTime = System.currentTimeMillis();
            Log4Test.trace("" + sere);
            boolean elementIsVisible = false;
            while (!elementIsVisible && (System.currentTimeMillis() - startTime) < (seconds * 1000)) {
                try {
                    new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(element));
                    elementIsVisible = element.isDisplayed();
                } catch (StaleElementReferenceException sere2) {
                    Log4Test.trace("" + sere2);
                }
            }
            if (!elementIsVisible) {
                testFailMessage("Element : " + elementName + " is not visible within : " + seconds + " seconds. It failed with StaleElementReferenceException");
            }
        }
    }

    public static void notVisibieElement(MobileDriver driver, MobileElement element, String elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to not be visible");
            new WebDriverWait(driver, 30).until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element : " + elementName + " is visible within : " + 30 + " seconds");
        } catch (NoSuchElementException nspe) {
            Log4Test.trace("" + nspe);
            testInfo("Element : " + elementName + " is no longer visible");
        }
    }

    public static void notVisibieElement(MobileDriver driver, MobileElement element, int timeoutTime, String
            elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to not be visible");
            new WebDriverWait(driver, timeoutTime).until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element : " + elementName + " is visible within : " + 30 + " seconds");
        } catch (NoSuchElementException nspe) {
            Log4Test.trace("" + nspe);
            testInfo("Element : " + elementName + " is no longer visible");
        }
    }

    public static void visibilityOfElement(MobileDriver driver, MobileElement element, String elementName,
                                           int timeoutTime) {
        try {
            testInfo("> " + elementName + " - waiting for element to be visible");
            new WebDriverWait(driver, timeoutTime).until(ExpectedConditions.visibilityOf(element));
        } catch (TimeoutException | NoSuchElementException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element : " + elementName + " is not visible within : " + timeoutTime + " seconds ");
        }
    }

    public static void visibilityOfElement(MobileDriver driver, List<MobileElement> element, int index, String
            elementName) {
        try {
            testInfo("> " + elementName + " - waiting for element to be visible");
            checkMobileElementListSizeIsNotEmpty(element, index);
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(element.get(index)));
        } catch (TimeoutException | IndexOutOfBoundsException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element : " + elementName + " is not visible within : " + 30 + " seconds ");
        }
    }

    public static void visibilityOfElement(MobileDriver driver, List<MobileElement> element, int index, String
            elementName, int timeoutTime) {
        try {
            testInfo("> " + elementName + " - waiting for element to be visible");
            checkMobileElementListSizeIsNotEmpty(element, index, timeoutTime);
            new WebDriverWait(driver, timeoutTime).until(ExpectedConditions.visibilityOf(element.get(index)));
        } catch (TimeoutException toe) {
            Log4Test.trace("" + toe);
            testFailMessage("Element : " + elementName + " is not visible within : " + timeoutTime + " seconds toe case");
        } catch (IndexOutOfBoundsException ioe) {
            Log4Test.trace("" + ioe);
            testFailMessage("Element : " + elementName + " is not visible within : " + timeoutTime + " seconds ioe case");
        }
    }

    public static void waitElementToBeEnabled(MobileElement element, String elementName, int seconds) {
        long startTime = System.currentTimeMillis();
        long waitTime = seconds * 1000;
        boolean enabledCondition = false;
        while (!enabledCondition && (System.currentTimeMillis() - startTime) < waitTime) {
            try {
                enabledCondition = element.isEnabled();
            } catch (NoSuchElementException e) {
                Log4Test.trace("" + e);
                testInfo("Element " + elementName + "is not enabled. Wait a little bit.");
                enabledCondition = false;
            }
        }
    }

}
