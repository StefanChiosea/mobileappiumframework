package com.projectName.appium.utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.projectName.appium.utils.commonMethods.DateUtil;
import com.projectName.appium.utils.enums.DateFormats;
import com.projectName.appium.utils.enums.Language;
import org.openqa.selenium.Platform;

import java.io.File;


//**********************************************************************************************************
public class ExtentManager {
    private static ExtentReports extent;
    private static Platform platform;

    private static String REPORTFILENAME = setProfileName();

    private static String MACPATH = System.getProperty("user.dir") + "/TestReport";
    private static String WINDOWSPATH = System.getProperty("user.dir") + "\\TestReport";
    private static String MACREPORTFILELOC = MACPATH + "/" + REPORTFILENAME;
    private static String WINREPORTFILEDOC = WINDOWSPATH + "\\" + REPORTFILENAME;

    public static ExtentReports getInstance() {
        if (extent == null) {
            createInstance();
        }
        return extent;
    }

    private static String setProfileName() {
        SetDevicesProps.setDevicesProps();
        return "Automated-Test-Report-" + setTimeStampReport() + "_" + System.getProperty("deviceRunOption") + ".html";
    }

    static String setTimeStampReport() {
        String currentDateTimestamp;
        if ("true".equals(System.getProperty("reportsTimestamp"))) {
            currentDateTimestamp = DateUtil.getCurrentDateInFormat(DateFormats.DATE_FORMAT_yyyy_MM_dd_DASH_TIMESTAMP, Language.RO).replace("T", "-T-").replace(":", "");
        } else {
            currentDateTimestamp = DateUtil.getCurrentDateInFormat(DateFormats.DATE_FORMAT_yyyy_MM_dd_DASH, Language.RO);
        }
        return currentDateTimestamp;
    }

    //Create an extent report instance
    public static ExtentReports createInstance() {

        platform = getCurrentPlatform();
        String fileName = getReportFileLocation(platform);
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
        htmlReporter.config().setCSS("css-string");
        htmlReporter.config().setJS("js-string");
        htmlReporter.config().setTimeStampFormat("MMM dd, yyyy HH:mm:ss");
        htmlReporter.config().setProtocol(Protocol.HTTPS);

        htmlReporter.config().setTheme(Theme.STANDARD);
        htmlReporter.config().setDocumentTitle(fileName);
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName(fileName);
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        if (Boolean.parseBoolean(System.getProperty("reportPortalActivate"))) {
            setReportPortalLaunchName();
        }

//        boolean setDateInNameCondition = Boolean.parseBoolean(System.getProperty("klovActivate"));
//        if (setDateInNameCondition) {
//            ExtentKlovReporter klov = new ExtentKlovReporter(getKlovProjectName(), getKlovTestName());
//            klov.initMongoDbConnection("10.231.243.244", 27017);
//            klov.initKlovServerConnection("http://10.231.243.244");
//            Log4Test.debug("Connected to klov server " + "http://10.231.243.244:55555");
//            Log4Test.debug("Connected to klov db " + "10.231.243.244:27017");
//            extent.attachReporter(klov);
//        }

        return extent;
    }

    //Select the extent report file location based on platform
    private static String getReportFileLocation(Platform platform) {
        String reportFileLocation = "defaultValue";
        switch (platform) {
            case MAC:
                reportFileLocation = MACREPORTFILELOC;
                createReportPath(MACPATH);
                Log4Test.info("ExtentReport Path for MAC: " + MACPATH + "\n");
                break;
            case WINDOWS:
                reportFileLocation = WINREPORTFILEDOC;
                createReportPath(WINDOWSPATH);
                Log4Test.info("ExtentReport Path for WINDOWS: " + WINDOWSPATH + "\n");
                break;
            default:
                Log4Test.info("ExtentReport path has not been set! There is a problem!\n");
                break;
        }
        return reportFileLocation;
    }

    //Create the report path if it does not exist
    private static void createReportPath(String path) {
        File testDirectory = new File(path);
        if (!testDirectory.exists()) {
            if (testDirectory.mkdir()) {
                Log4Test.info("Directory: " + path + " is created!");
            } else {
                Log4Test.info("Failed to create directory: " + path);
            }
        } else {
            Log4Test.info("Directory already exists: " + path);
        }
    }

    //Get current platform
    static Platform getCurrentPlatform() {
        if (platform == null) {
            String operSys = System.getProperty("os.name").toLowerCase();
            if (operSys.contains("win")) {
                platform = Platform.WINDOWS;
            } else if (operSys.contains("nix") || operSys.contains("nux")
                    || operSys.contains("aix")) {
                platform = Platform.LINUX;
            } else if (operSys.contains("mac")) {
                platform = Platform.MAC;
            }
        }
        return platform;
    }

    public static void setReportPortalLaunchName() {
        System.setProperty("rp.launch", getReportPortalTestName());
        Log4Test.debug("rp.launch - Report Portal Test Name: " + System.getProperty("rp.launch"));
    }

    private static String getKlovProjectName() {
        String device = System.getProperty("driverType");
        String value;
        String xmlName = System.getProperty("suiteXmlFile");
        if (xmlName != null) {
            if (xmlName.contains("Sanity")) {
                value = "Sanity " + "_" + device + "_" + System.getProperty("appEnv");
            } else if (xmlName.contains("Smoke")) {
                value = "Smoke " + "_" + device + "_" + System.getProperty("appEnv");
            } else {
                value = "Regression " + "_" + device + "_" + System.getProperty("appEnv");
            }
        } else {
            value = device + "_" + System.getProperty("appEnv");
        }

        return value;
    }

    private static String getKlovTestName() {
        String xmlName = System.getProperty("suiteXmlFile");
        if (xmlName != null) {
            xmlName = "_Suite Xml run: " + xmlName;
        } else {
            xmlName = "";
        }
        return setTimeStampReportKlov() + "_" + System.getProperty("languageProp") + "_" + System.getProperty("deviceRunOption") + xmlName;
    }

    private static String getReportPortalTestName() {
        String xmlName = System.getProperty("suiteXmlFile");
        //Log4Test.debug("xmlName " + xmlName);
        if (xmlName != null) {
            xmlName = "|Suite Xml run: " + xmlName;
        } else {
            xmlName = "";
        }
        return setTimeStampReportKlov() + "|env: " + System.getProperty("appEnv") + "|lang: " + System.getProperty("languageProp") + "|device: " + System.getProperty("deviceRunOption") + xmlName;
    }

    private static String setTimeStampReportKlov() {
        return DateUtil.getCurrentDateInFormat(DateFormats.DATE_FORMAT_yyyy_MM_dd_DASH_TIMESTAMP, Language.RO).replace("T", "-T-").replace(":", "");
    }

}