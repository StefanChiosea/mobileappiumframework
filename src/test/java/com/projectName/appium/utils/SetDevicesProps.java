package com.projectName.appium.utils;

import com.projectName.appium.listeners.ExtentTestListener;
import org.openqa.selenium.Platform;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class SetDevicesProps {
    public static Properties projectProperties = new Properties();

    private static String MACPATH_PROPS = System.getProperty("user.dir") + "/src/test/resources/";
    private static String WINDOWSPATH_PROPS = System.getProperty("user.dir") + "\\src\\test\\resources\\";
    private static String MACPATH_PROPS_RP = System.getProperty("user.dir") + "/src/main/resources/";
    private static String WINDOWSPATH_PROPS_RP = System.getProperty("user.dir") + "\\src\\main\\resources\\";
    private static String PROPS_PATH;
    private static String PROPS_PATH_RP;
    private static Platform platform;

    static void setDevicesProps() {
        setPropsFileLocation();
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.getProperties().store(baos, "system props");
            InputStream input0 = new ByteArrayInputStream(baos.toByteArray());
            FileInputStream input1 = new FileInputStream(PROPS_PATH + "userProp.properties");
            FileInputStream input2 = new FileInputStream(PROPS_PATH + "envProp.properties");
            FileInputStream input3 = new FileInputStream(PROPS_PATH_RP + "reportportal.properties");

            projectProperties.load(input0);
            projectProperties.load(input1);
            projectProperties.load(input2);
            projectProperties.load(input3);
            System.setProperty("logLevel", projectProperties.getProperty("logLevel"));

            for (String localPropName : projectProperties.stringPropertyNames()) {
                String systemPropValue = System.getProperty(localPropName);
                String localPropValue = projectProperties.getProperty(localPropName);
                if (systemPropValue == null && localPropValue != null) {
                    Log4Test.debug("------------------------ localPropName ------------------------  " + localPropName);
                    Log4Test.debug("System prop value was:  " + null);
                    Log4Test.debug("Local prop value is:  " + localPropValue);
                    System.setProperty(localPropName, localPropValue);
                    Log4Test.debug("System prop value after set is:  " + System.getProperty(localPropName));
                }
            }
        } catch (java.io.IOException e) {
            Log4Test.trace("" + e);
        }

        if (!Boolean.parseBoolean(System.getProperty("jenkinsRun"))) {
            System.setProperty("appiumServerLocation", "http://127.0.0.1");

            String deviceRunOption = System.getProperty("deviceRunOption");
            Log4Test.info("Setting property for: " + deviceRunOption);
            switch (deviceRunOption) {
                case "a1":
                    System.setProperty("deviceUDID", "5200ad148fdb5581");
                    System.setProperty("udid", "5200ad148fdb5581");
                    System.setProperty("deviceName", "Galaxy j5");
                    System.setProperty("platformVersion", "8.1");
                    System.setProperty("driverType", "ANDROID");
                    System.setProperty("systemPort", String.valueOf(8300));
                    break;
                case "i1":
                    System.setProperty("deviceUDID", "GAAXGY03P5316VE");
                    System.setProperty("udid", "GAAXGY03P5316VE");
                    System.setProperty("deviceName", "iPhone X");
                    System.setProperty("platformVersion", "13");
                    System.setProperty("driverType", "IOS");
                    System.setProperty("systemPort", String.valueOf(8500));
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + deviceRunOption);
            }

            Log4Test.info("Setting property deviceName: " + System.getProperty("deviceName"));
            Log4Test.info("Setting property platformVersion: " + System.getProperty("platformVersion"));
            Log4Test.info("Setting property driverType: " + System.getProperty("driverType"));
            Log4Test.info("Setting property appiumServerLocation: " + System.getProperty("appiumServerLocation"));
            String device = System.getProperty("driverType");
            switch (device) {
                case "ANDROID":
                case "ANDROID_SMART_TOKEN":
                    Log4Test.info("Setting property systemPort: " + System.getProperty("systemPort"));
                    break;
                case "IOS":
                case "IOS_SMART_TOKEN":
                    Log4Test.info("Setting property wdaLocalPort: " + System.getProperty("wdaLocalPort"));
                    break;
                default:
                    ExtentTestListener.testFailMessage("Wrong device param " + device);
            }
        }

    }

    private static void setPropsFileLocation() {
        Platform platform = getCurrentPlatform();
        String propsFileLocation = "defaultValue";
        String propsFileLocation_rp = "defaultValue";
        switch (platform) {
            case MAC:
                propsFileLocation = MACPATH_PROPS;
                propsFileLocation_rp = MACPATH_PROPS_RP;
                break;
            case WINDOWS:
                propsFileLocation = WINDOWSPATH_PROPS;
                propsFileLocation_rp = WINDOWSPATH_PROPS_RP;
                break;
            default:
                Log4Test.info("Props path has not been set! There is a problem!\n");
                break;
        }
        PROPS_PATH = propsFileLocation;
        PROPS_PATH_RP = propsFileLocation_rp;

    }

    //Get current platform
    private static Platform getCurrentPlatform() {
        if (platform == null) {
            String operSys = System.getProperty("os.name").toLowerCase();
            if (operSys.contains("win")) {
                platform = Platform.WINDOWS;
            } else if (operSys.contains("nix") || operSys.contains("nux")
                    || operSys.contains("aix")) {
                platform = Platform.LINUX;
            } else if (operSys.contains("mac")) {
                platform = Platform.MAC;
            }
        }
        return platform;
    }
}
