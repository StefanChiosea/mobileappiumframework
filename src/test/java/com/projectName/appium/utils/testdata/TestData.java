package com.projectName.appium.utils.testdata;

public class TestData {

    // android app package
    public static final String APP_PACKAGE = "";
    // android app activity
    public static final String APP_ACTIVITY = "";
    // ios app package
    public static final String BUNDLE_ID = "";


    // Wait time

    public static final int SHORT_WAIT = 7;
    public static final int MEDIUM_WAIT = 15;
    public static final int LONG_WAIT = 60;
}
