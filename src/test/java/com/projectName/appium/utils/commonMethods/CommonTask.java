package com.projectName.appium.utils.commonMethods;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.projectName.appium.listeners.ExtentTestListener;
import com.projectName.appium.utils.Log4Test;
import com.projectName.appium.utils.testdata.TestData;
import com.projectName.appium.utils.waiting.Waiting;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.offset.PointOption.point;


/**
 * Created by azaharia on 28.04.2017.
 */
public class CommonTask extends ExtentTestListener {


    public static void waitForLoaderToFinish(MobileElement element) {
        String device = System.getProperty("driverType");
        long startTime = System.currentTimeMillis();
        switch (device) {
            case "ANDROID":
                try {
                    while (element.isDisplayed() && (System.currentTimeMillis() - startTime) < 35000) {
                        element.isDisplayed();
                    }
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    Log4Test.trace("" + e);
                    testInfo("Loader is finished");
                }
                break;
            case "IOS":
                try {
                    while (element.isEnabled() && (System.currentTimeMillis() - startTime) < 35000) {
                        element.isEnabled();
                    }
                } catch (NoSuchElementException e) {
                    Log4Test.trace("" + e);
                    testInfo("Loader is finished");
                }
                break;
            default:
                testFailMessage("Wrong device param " + device);
        }

    }

    public static void longSwipeIntoViewVertical(MobileDriver driver, MobileElement element, String elementName) {
        boolean status = true;
        testInfo("> " + elementName + " - scrolling to ");
        long startTime = System.currentTimeMillis();
        while (status && (System.currentTimeMillis() - startTime) < 25000) {
            try {
                element.isDisplayed();
                status = false;
            } catch (NoSuchElementException e) {
                Log4Test.trace("" + e);
                try {
                    longSwipeVertical(driver);
                } catch (WebDriverException wde) {
                    Log4Test.trace("" + wde);
                    longSwipeVertical(driver);
                }
            }
        }
    }

    public static void longSwipeIntoViewVertical(MobileDriver driver, List<MobileElement> element, int index, String elementName) {
        boolean status = true;
        testInfo("> " + elementName + " - scrolling to ");
        long startTime = System.currentTimeMillis();
        while (status && (System.currentTimeMillis() - startTime) < 25000) {
            try {
                element.get(index).isDisplayed();
                status = false;
            } catch (NoSuchElementException | IndexOutOfBoundsException e) {
                Log4Test.trace("" + e);
                try {
                    longSwipeVertical(driver);
                } catch (WebDriverException wde) {
                    Log4Test.trace("" + wde);
                    longSwipeVertical(driver);
                }
            }
        }
    }

    public static void longSwipeVertical(MobileDriver driver, int times) {
        for (int i = 0; i < times; i++) {
            longSwipeVertical(driver);
        }
    }

    public static void shortSwipeVertical(MobileDriver driver, int times) {
        for (int i = 0; i < times; i++) {
            shortSwipeVertical(driver);
        }
    }

    public static void longSwipeBottomToUp(MobileDriver driver) {
        Dimension size = driver.manage().window().getSize();
        for (int i = 0; i < 1; i++) {
            int anchor = (int) (size.width * 0.5);
            int startPoint = (int) (size.height * 0.7);
            int endPoint = (int) (size.height * 0.4);
            new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(500))).moveTo(PointOption.point(endPoint, startPoint + 300)).release().perform();
        }
    }

    /*public static void shortSwipeVerticalByAccountSize(MobileDriver driver, Map<String, String> cookies, EndPoint endpoint, MobileElement element, String elementName) {
        //MyCardsAllCardsPage myCardsDPAllCardsPage = new MyCardsAllCardsPage((AppiumDriver) driver);
        int accountListSize = CommonRestTask.getJsonPathFromWSResponse(endpoint, cookies).param("idParam", "133").getList("findAll {it.productId != idParam}").size();
        int scrollSize = accountListSize + myCardsDPAllCardsPage.getCreditCardListSizeWsForDashboard();
        testDebugMessage("scrollable list size is " + scrollSize);
        MainPage.setAccountListSize(scrollSize);
        String device = System.getProperty("driverType");
        Dimension size = driver.manage().window().getSize();
        if (scrollSize > 4) {
            switch (device) {
                case "ANDROID":
                    for (int i = 0; i < scrollSize; i++) {
                        try {
                            element.isDisplayed();
                            break;
                        } catch (NoSuchElementException nse) {
                            Log4Test.trace("" + nse);
                            setScrollParams(driver, size, 0.5, 0.8, 0.2, 500);
                        }
                    }
                    break;
                case "IOS":
                    for (int i = 0; i < scrollSize; i++) {
                        boolean elementStatus = element.isDisplayed();
                        if (elementStatus) {
                            testDebugMessage("Element is displayed? " + elementStatus + " , scroll ended");
                            setScrollParams(driver, size, 0.5, 0.8, 0.3, 2000);
                            break;
                        }
                        testDebugMessage("Element is displayed? " + elementStatus + " , continue scrolling");
                        setScrollParams(driver, size, 0.5, 0.8, 0.3, 2000);
                    }
                    break;
                default:
                    testFailMessage("Wrong device param " + device);
            }
        }
    }*/

    private static void setScrollParams(MobileDriver driver, Dimension size, Double anchorP, Double startPointP, Double endPointP, int duration) {
        Log4Test.trace("doing a swipeling swipe swipe long vertical \t ♪♬ ᕕ(⌐■_■)ᕗ ♪♬");
        int anchor = (int) (size.width * anchorP);
        int startPoint = (int) (size.height * startPointP);
        int endPoint = (int) (size.height * endPointP);
        try {
            new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
        } catch (WebDriverException wde) {
            Log4Test.trace("" + wde);
            new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
        }
    }

    public static void longSwipeVertical(MobileDriver driver) {
        Dimension size = driver.manage().window().getSize();
        for (int i = 0; i < 2; i++) {
            Log4Test.trace("doing a swipeling swipe swipe long vertical \t♪♬ ᕕ(⌐■_■)ᕗ ♪♬");
            int anchor = (int) (size.width * 0.5);
            int startPoint = (int) (size.height * 0.9);
            int endPoint = (int) (size.height * 0.1);
            try {
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(3000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            } catch (WebDriverException wde) {
                Log4Test.trace("" + wde);
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(3000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            }
            //In documentation they mention moveTo coordinates are relative to initial ones, but thats not happening. When it does we need to use the function below
            //new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(duration)).moveTo(PointOption.point(0,endPoint-startPoint)).release().perform();
        }

    }

    public static void shortSwipeVertical(MobileDriver driver) {
        Dimension size = driver.manage().window().getSize();
        for (int i = 0; i < 2; i++) {
            Log4Test.trace("doing a swipeling swipe swipe short vertical \t♪♬ ᕕ(⌐■_■)ᕗ ♪♬");
            int anchor = (int) (size.width * 0.5);
            int startPoint = (int) (size.height * 0.7);
            int endPoint = (int) (size.height * 0.4);
            try {
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            } catch (WebDriverException wde) {
                Log4Test.trace("" + wde);
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            }
            //In documentation they mention moveTo coordinates are relative to initial ones, but thats not happening. When it does we need to use the function below
            //new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(duration)).moveTo(PointOption.point(0,endPoint-startPoint)).release().perform();
        }

    }

    public static void shortSwipeVerticalReverse(MobileDriver driver) {
        Dimension size = driver.manage().window().getSize();
        for (int i = 0; i < 2; i++) {
            Log4Test.trace("doing a swipeling swipe swipe short vertical reverse \t♬♪ ᕕ(■_■⌐)ᕗ ♪♬");
            int anchor = (int) (size.width * 0.5);
            int startPoint = (int) (size.height * 0.4);
            int endPoint = (int) (size.height * 0.7);
            try {
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            } catch (WebDriverException wde) {
                Log4Test.trace("" + wde);
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            }
            //In documentation they mention moveTo coordinates are relative to initial ones, but thats not happening. When it does we need to use the function below
            //new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(duration)).moveTo(PointOption.point(0,endPoint-startPoint)).release().perform();
        }

    }

    public static void swipeVertical(MobileDriver driver, double startPercentage, double finalPercentage,
                                     double anchorPercentage, int duration) {
        Dimension size = driver.manage().window().getSize();
        for (int i = 0; i < 2; i++) {
            Log4Test.trace("doing a swipeling swipe swipe vertical with params \t♪♬ ᕕ(⌐■_■)ᕗ ♪♬");
            int anchor = (int) (size.width * anchorPercentage);
            int startPoint = (int) (size.height * startPercentage);
            int endPoint = (int) (size.height * finalPercentage);
            try {
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            } catch (WebDriverException wde) {
                Log4Test.trace("" + wde);
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            }
            //In documentation they mention moveTo coordinates are relative to initial ones, but thats not happening. When it does we need to use the function below
            //new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(duration)).moveTo(PointOption.point(0,endPoint-startPoint)).release().perform();
        }
    }

    public static void swipeVertical(MobileDriver driver, double startPercentage, double finalPercentage,
                                     double anchorPercentage, int duration, int swipeTimes) {
        for (int i = 0; i < swipeTimes; i++) {
            Log4Test.trace("doing a swipeling swipe swipe vertical with params and times " + swipeTimes + " \t♪♬ ᕕ(⌐■_■)ᕗ ♪♬");
            Dimension size = driver.manage().window().getSize();
            int anchor = (int) (size.width * anchorPercentage);
            int startPoint = (int) (size.height * startPercentage);
            int endPoint = (int) (size.height * finalPercentage);
            try {
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            } catch (WebDriverException wde) {
                Log4Test.trace("" + wde);
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            }
            //In documentation they mention moveTo coordinates are relative to initial ones, but thats not happening. When it does we need to use the function below
            //new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(duration)).moveTo(PointOption.point(0,endPoint-startPoint)).release().perform();
        }
    }

    public static void swipeHorizontal(MobileDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration) {
        Log4Test.trace("doing a swipeling swipe swipe horizontal \t♪♬ ᕕ(⌐■_■)ᕗ ♪♬");
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.height * anchorPercentage);
        int startPoint = (int) (size.width * startPercentage);
        int endPoint = (int) (size.width * finalPercentage);

        try {
            new TouchAction(driver).press(PointOption.point(startPoint, anchor)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(endPoint, anchor)).release().perform();
        } catch (WebDriverException wde) {
            Log4Test.trace("" + wde);
            new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
        }
        //In documentation they mention moveTo coordinates are relative to initial ones, but thats not happening. When it does we need to use the function below
        //new TouchAction(driver).press(PointOption.point(startPoint, anchor)).waitAction(duration)).moveTo(PointOption.point(endPoint-startPoint,0)).release().perform();
    }

    public static void swipeHorizontalByTimes(MobileDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration, int swipeTimes) {
        for (int i = 0; i < swipeTimes; i++) {
            Log4Test.trace("doing a swipeling swipe swipe horizontal \t♪♬ ᕕ(⌐■_■)ᕗ ♪♬");
            Dimension size = driver.manage().window().getSize();
            int anchor = (int) (size.height * anchorPercentage);
            int startPoint = (int) (size.width * startPercentage);
            int endPoint = (int) (size.width * finalPercentage);

            try {
                new TouchAction(driver).press(PointOption.point(startPoint, anchor)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(endPoint, anchor)).release().perform();
            } catch (WebDriverException wde) {
                Log4Test.trace("" + wde);
                new TouchAction(driver).press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(PointOption.point(anchor, endPoint)).release().perform();
            }
            //In documentation they mention moveTo coordinates are relative to initial ones, but thats not happening. When it does we need to use the function below
            //new TouchAction(driver).press(PointOption.point(startPoint, anchor)).waitAction(duration)).moveTo(PointOption.point(endPoint-startPoint,0)).release().perform();
        }
    }

    public static void moveToElement(MobileDriver driver, MobileElement element, String elementName) {
        try {
            Actions action = new Actions(driver);
            testInfo("> " + elementName + " - moving to element");
            action.moveToElement(element).build().perform();
        } catch (NoSuchElementException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element is not found : " + elementName);
        }
    }


    public static void clearElement(MobileElement element, String elementName) {
        try {
            testInfo("> " + elementName + " - clearing element");
            element.clear();
        } catch (NoSuchElementException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element is not found : " + elementName);
        }
    }

    public static void clearElement(List<MobileElement> element, int index, String elementName) {
        try {
            testInfo("> " + elementName + " - clearing element");
            checkMobileElementListSizeIsNotEmpty(element, index);
            element.get(index).clear();
        } catch (NoSuchElementException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element is not found : " + elementName);
        }
    }

    public static void sendKeys(MobileElement element, String text, String elementName) {
        String device = System.getProperty("driverType");
        switch (device) {
            case "ANDROID":
                try {
                    testInfo("> " + elementName + " - try sending keys : " + text);
                    element.sendKeys(text);
                } catch (NoSuchElementException e) {
                    Log4Test.trace("" + e);
                    Assert.fail("Element is not found : " + elementName);
                } catch (WebDriverException wde) {
                    Log4Test.trace("" + wde);
                    testInfo("> Sending keys didn't work");
                    sendKeysActions(getDriver(), element, text, elementName);
                }
                break;
            case "IOS":
                try {
                    testInfo("> " + elementName + " - try setting value : " + text);
                    try {
                        element.setValue(text);
                    } catch (NotFoundException nfe) {
                        Log4Test.trace("" + nfe);
                        testWarnMessage("Keyboard might not be displayed!");
                    }
                } catch (NoSuchElementException e) {
                    Log4Test.trace("" + e);
                    Assert.fail("Element is not found : " + elementName);
                }
                break;
            default:
                testFailMessage("Wrong device param " + device);
        }
    }

    public static void tapDoneKeyboard(MobileDriver driver, MobileElement element, String elementName) {
        try {
            testInfo("> " + elementName + " - tapping element");
            new TouchAction(driver).tap(element(element)).perform();
        } catch (NoSuchElementException | AssertionError e) {
            Log4Test.trace("" + e);
            testFailMessage(elementName + " Not found");
        }
    }

    public static void tapButton(MobileDriver driver, MobileElement element, String elementName) {
        String device = System.getProperty("driverType");
        switch (device) {
            case "ANDROID":
                try {
                    testInfo("> " + elementName + " - tapping element");
                    new TouchAction(driver).tap(element(element)).perform();
                } catch (NoSuchElementException e) {
                    Log4Test.trace("" + e);
                    testFailMessage(elementName + " Not found");
                }
                break;
            case "IOS":
                tapElementiOS(driver, element, elementName);
                break;
            default:
                testFailMessage("Wrong device param " + device);
        }
    }

    private static void tapElementiOS(MobileDriver driver, MobileElement element, String elementName) {
        // Locate center of element
        testInfo("> " + elementName + " - getting element dimensions for ios tap");
        try {
            Point location = element.getLocation();
            Dimension size = element.getSize();
            int tapX = location.getX() + (size.getWidth() / 2);
            int tapY = location.getY() + (size.getHeight() / 2);
            testDebugMessage("Tapping za x: " + tapX);
            testDebugMessage("Tapping za y: " + tapY);
            // Execute tap
            testInfo("> " + elementName + " - tapping element");
            TouchAction action = new TouchAction(driver);
            action.tap(point(tapX, tapY)).perform();
        } catch (WebDriverException nse) {
            Log4Test.trace("" + nse);
            testFailMessage("Element is not found : " + elementName);
        }
    }

    public static void tapButton(MobileDriver driver, List<MobileElement> list, int index, String elementName) {
        String device = System.getProperty("driverType");
        switch (device) {
            case "ANDROID":
                try {
                    testInfo("> " + elementName + " - tapping element");
                    checkMobileElementListSizeIsNotEmpty(list, index);
                    new TouchAction(driver).tap(element(getElementFromList(list, index, elementName))).perform();
                } catch (NoSuchElementException e) {
                    Log4Test.trace("" + e);
                    Assert.fail("Element is not found : " + elementName);
                }
                break;
            case "IOS":
                tapElementiOS(driver, list.get(index), elementName);
                break;
            default:
                testFailMessage("Wrong device param " + device);
        }
    }


    public static void tryTapButton(MobileDriver driver, MobileElement element, String elementName) {
        try {
            testInfo("> " + elementName + " - tapping element");
            new TouchAction(driver).tap(element(element)).perform();
        } catch (NoSuchElementException | ElementNotVisibleException e) {
            Log4Test.trace("" + e);
            testDebugMessage("Element is not found " + elementName);
        }
    }

    public static void tryTapButton(MobileDriver driver, List<MobileElement> element, int index, String elementName) {
        try {
            testInfo("> " + elementName + " - tapping element");
            new TouchAction(driver).tap(element(element.get(index))).perform();
        } catch (NoSuchElementException | ElementNotVisibleException | IndexOutOfBoundsException e) {
            Log4Test.trace("" + e);
            testDebugMessage("Element is not found " + elementName);
        }
    }


    public static void moveElementToElement(MobileDriver driver, MobileElement elementToBeMoved, String firstElementName, MobileElement arrivingElement, String secondElementName) {
        testInfo("- moving element " + firstElementName + " to " + secondElementName);
        String device = System.getProperty("driverType");
        switch (device) {
            case "ANDROID":
                try {
                    TouchAction action = new TouchAction(driver);
                    action.longPress(element(elementToBeMoved)).moveTo(element(arrivingElement)).release().perform();
                } catch (NoSuchElementException e) {
                    Log4Test.trace("" + e);
                    Assert.fail("Element is not found : " + firstElementName + "or" + secondElementName);
                }
                break;
            case "IOS":
                try {
                    TouchAction action = new TouchAction(driver);
                    action.longPress(element(elementToBeMoved)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(3000))).moveTo(element(arrivingElement)).release().perform();
                } catch (NoSuchElementException e) {
                    Log4Test.trace("" + e);
                    Assert.fail("Element is not found : " + firstElementName + "or" + secondElementName);
                }
                break;
            default:
                testFailMessage("Wrong device param " + device);
        }
    }

    public static void setValue(MobileElement element, String text, String elementName) {
        try {
            testInfo("> " + elementName + " - set value : " + text);
            element.clear();
            element.setValue(text);
        } catch (NoSuchElementException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element is not found : " + elementName);
        }
    }


    public static void setTextField(MobileDriver driver, MobileElement element, String text, String elementName) {

        //Wait for the field to be available
        Waiting.elementToBeClickable(driver, element, elementName);
        //Clear field
        clearElement(element, elementName);
        //Enter "item" into the field
        sendKeys(element, text, elementName);
        //Waiting text to be present
        //Waiting.textToBePresentInElement(driver, element, text, elementName);
    }

    public static void setInputField(MobileDriver driver, MobileElement element, String text, String elementName) {
        Waiting.elementToBeClickable(driver, element, elementName);
        clearElement(element, elementName);
        sendKeys(element, text, elementName);
    }

    public static void setInputField(MobileDriver driver, MobileElement element, int duration, String text, String elementName) {
        Waiting.elementToBeClickable(driver, element, duration, elementName);
        clearElement(element, elementName);
        sendKeys(element, text, elementName);
    }

    public static void sendKeysActions(MobileDriver driver, MobileElement element, String text, String elementName) {
        Waiting.elementToBeClickable(driver, element, elementName);
        clearElement(element, elementName);
        Actions actions = new Actions(driver);
        testInfo("> " + elementName + " - sending keys actions : " + text);
        actions.sendKeys(element, text);
        actions.perform();
    }

    public static void hideKeyboard(MobileDriver driver) {
        String device = System.getProperty("driverType");
        switch (device) {
            case "ANDROID":
                try {
                    testInfo("Try to hide keyboard!");
                    driver.hideKeyboard();
                } catch (WebDriverException we) {
                    testInfo("Soft keyboard not present");
                }
                break;
            case "IOS":
                break;
            default:
                Assert.fail("Wrong device type");
        }
    }

    public static String getText(MobileElement element, String elementName) {
        String text = "defaultValue";
        String device = System.getProperty("driverType");
        switch (device) {
            case "ANDROID":
                Waiting.visibilityOfElement(getDriver(), element, elementName, 5);
                try {
                    testInfo("> " + elementName + " - getting text");
                    text = element.getText();
                } catch (NoSuchElementException e) {
                    Log4Test.trace("" + e);
                    testFailMessage("Element is not found : " + elementName);
                }
                break;
            case "IOS":
                try {
                    testInfo("> " + elementName + " - getting text by attribute label");
                    text = element.getAttribute("label");
                    if (text == null) {
                        testWarnMessage("UPDATE YOUR LOCATORS ACCORDINGLY OR GET ELEMENT ATTRIBUTE FOR THAT PARTICULAR ELEMENT !!! ");
                    }
                } catch (NoSuchElementException nse) {
                    Log4Test.trace("" + nse);
                    testFailMessage("Element is not found : " + elementName);
                }
                break;
            default:
                testFailMessage("Wrong device param " + device);
        }
        return text;
    }

    public static String getTextIosByAttribute(List<MobileElement> list, int index, String
            elementAttribute, String elementName) {
        String text = "defaultValue";
        try {
            testInfo("> " + elementName + " - getting text");
            checkMobileElementListSizeIsNotEmpty(list, index);
            text = getElementFromList(list, index, elementName).getAttribute(elementAttribute);
        } catch (NoSuchElementException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element is not found : " + elementName);
        }

        return text;

    }

    public static void checkMobileElementListSizeIsNotEmpty(List<MobileElement> list, int index) {
        long startTime = System.currentTimeMillis();
        long waitTime = TestData.MEDIUM_WAIT * 1000;
        boolean mobileElementListSizeBiggerThanIndex = list.size() > index;
        testDebugMessage("Element list size is " + list.size());
        testDebugMessage("while master condition : " + (!mobileElementListSizeBiggerThanIndex && ((System.currentTimeMillis() - startTime) < waitTime)));
        while (!mobileElementListSizeBiggerThanIndex && ((System.currentTimeMillis() - startTime) < waitTime)) {
            testDebugMessage("Element list size is " + list.size() + " and is not bigger than index " + index + " lets wait a sec and try again.");
            mobileElementListSizeBiggerThanIndex = list.size() > index;
            testDebugMessage("Element list size is " + list.size());
            testDebugMessage("While condition: " + mobileElementListSizeBiggerThanIndex);
            testDebugMessage("while master condition : " + (!mobileElementListSizeBiggerThanIndex && ((System.currentTimeMillis() - startTime) < waitTime)));
        }
    }

    public static void checkMobileElementListSizeIsNotEmpty(List<MobileElement> list, int index, int seconds) {
        long startTime = System.currentTimeMillis();
        long waitTime = seconds * 1000;
        boolean mobileElementListSizeBiggerThanIndex = list.size() > index;
        testDebugMessage("Element list size is " + list.size());
        testDebugMessage("while master condition : " + (!mobileElementListSizeBiggerThanIndex && ((System.currentTimeMillis() - startTime) < waitTime)));
        while (!mobileElementListSizeBiggerThanIndex && ((System.currentTimeMillis() - startTime) < waitTime)) {
            testDebugMessage("Element list size is " + list.size() + " and is not bigger than index " + index + " lets wait a sec and try again.");
            mobileElementListSizeBiggerThanIndex = list.size() > index;
        }
    }

    public static String getText(List<MobileElement> list, int index, String elementName) {
        String text = "defaultValue";
        String device = System.getProperty("driverType");
        checkMobileElementListSizeIsNotEmpty(list, index);
        switch (device) {
            case "ANDROID":
                try {
                    testInfo("> " + elementName + " - getting text");
                    text = getElementFromList(list, index, elementName).getText();
                } catch (IndexOutOfBoundsException e) {
                    Log4Test.trace("" + e);
                    Assert.fail("Element is not found : " + elementName);
                }
                break;
            case "IOS":
                try {
                    testInfo("> " + elementName + " - getting text by attribute label");
                    text = getElementFromList(list, index, elementName).getAttribute("label");
                    if (text == null) {
                        testWarnMessage("UPDATE YOUR LOCATORS ACCORDINGLY OR GET ELEMENT ATTRIBUTE FOR THAT PARTICULAR ELEMENT !!! ");
                    }
                } catch (NoSuchElementException | IndexOutOfBoundsException nse) {
                    Log4Test.trace("" + nse);
                    testFailMessage("Element is not found : " + elementName);
                }
                break;
            default:
                testFailMessage("Wrong device param " + device);
        }
        return text;
    }


    public static String getAttributeAsText(MobileElement element, String attribute, String elementName) {
        String stringValueOfAttribute = "defaultValue";
        try {
            //Get element Attribute
            testInfo("- getting attribute " + attribute + " as String for : " + elementName);
            stringValueOfAttribute = element.getAttribute(attribute);
        } catch (NoSuchElementException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element is not found : " + elementName);
        }

        return stringValueOfAttribute;
    }

    public static String getAttributeAsText(List<MobileElement> list, int index, String attribute, String elementName) {
        String stringValueOfAttribute = "defaultValue";
        checkMobileElementListSizeIsNotEmpty(list, index);
        try {
            //Get element Attribute
            testInfo("- getting attribute " + attribute + " as String for : " + elementName);
            stringValueOfAttribute = list.get(index).getAttribute(attribute);
        } catch (NoSuchElementException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element is not found : " + elementName);
        }

        return stringValueOfAttribute;
    }


    public static Boolean isElementEnabledAndDisplayed(MobileElement element, String elementName) {
        try {
            return isDisplayed(element, elementName) && isEnabled(element, elementName);
        } catch (NoSuchElementException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element is not found : " + elementName);
            return false;
        }
    }

    public static Boolean isEnabled(MobileElement element, String elementName) {
        try {
            testInfo("> " + elementName + " - verifying if enabled");
            return element.isEnabled();
        } catch (NoSuchElementException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element is not found : " + elementName);
            return false;
        }
    }

    public static boolean isSelected(MobileElement element, String elementName) {
        String device = System.getProperty("driverType");
        boolean status = false;
        switch (device) {
            case "ANDROID":
                try {
                    testInfo("> " + elementName + " - verifying if selected");
                    status = "true".equals(element.getAttribute("selected"));
                } catch (NoSuchElementException e) {
                    testFailMessage("Element is not found : " + elementName);
                    return false;
                }
                break;
            case "IOS":
                try {
                    testInfo("> " + elementName + " - verifying if selected");
                    status = "1".equals(element.getAttribute("value"));
                } catch (NullPointerException npe) {
                    Log4Test.trace("" + npe);
                    testDebugMessage("Element is not selected : " + elementName);
                    status = false;
                } catch (NoSuchElementException e) {
                    testFailMessage("Element is not found : " + elementName);
                }
                break;
            default:
                testFailMessage("Wrong device param " + device);
        }
        return status;
    }

    public static Boolean isDisplayed(MobileElement element, String elementName) {
        try {
            testInfo("> " + elementName + " - verifying if displayed");
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            Assert.fail("Element is not found : " + elementName);
            return false;
        }
    }

    public static Boolean isDisplayed(List<MobileElement> list, int index, String elementName) {
        boolean status = false;
        checkMobileElementListSizeIsNotEmpty(list, index);
        try {
            testInfo("> " + elementName + " - getting element status");
            status = getElementFromList(list, index, elementName).isDisplayed();
        } catch (NoSuchElementException | IndexOutOfBoundsException nse) {
            Log4Test.trace("" + nse);
            testFailMessage("Element is not found : " + elementName);
        }
        return status;


    }


    public static MobileElement getElementFromList(List<MobileElement> list, int index, String objectName) {
        try {
            checkMobileElementListSizeIsNotEmpty(list, index);
            //noinspection ResultOfMethodCallIgnored
            list.get(index);
        } catch (IndexOutOfBoundsException | StaleElementReferenceException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element not found in list, cause list is empty for " + objectName);
        }
        return list.get(index);
    }

    public static JsonObject getElementFromList(JsonArray list, int index, String objectName) {
        try {
            list.get(index);
        } catch (IndexOutOfBoundsException e) {
            Log4Test.trace("" + e);
            Assert.fail("Element not found in list, cause list is empty for " + objectName);
        }
        return list.get(index).getAsJsonObject();
    }

    //driver, element, elementName
    //driver, list, index, elementName


    public static Boolean isCounterWorking(MobileElement inputFieldElement, MobileElement counterElement,
                                           int counterStartsAt, String elementName) {

        boolean counterReflectsRemainingChars = true;
        CommonTask.sendKeys(inputFieldElement, "0", elementName + " field");

        for (int i = counterStartsAt; i >= 0 && counterReflectsRemainingChars; i--) {

            Log4Test.info("Test counter for : " + i + " remaining chars");

            //Get lenght of inputed string
            int inputStringLenght = counterStartsAt - CommonTask.getText(inputFieldElement, elementName + " field").length();

            //Get counter int value
            int counterValue = Integer.parseInt(CommonTask.getText(counterElement, elementName + " counter").replaceAll("[^0-9]", ""));

            //Send new char to input field
            CommonTask.sendKeys(inputFieldElement, "0", elementName + " field");

            //Compare if counter reflects remaining chars
            counterReflectsRemainingChars = counterValue == inputStringLenght;

        }
        return counterReflectsRemainingChars;
    }

    public static void addStringToTxtFile(String textToAdd, String txtFileNamePath) {
        try {
            File file = new File(txtFileNamePath);
            FileWriter fr;
            fr = new FileWriter(file, true);
            BufferedWriter br = new BufferedWriter(fr);
            br.write("\n" + textToAdd);

            br.close();
            fr.close();
        } catch (IOException e) {
            testInfo("" + e);
        }
    }


    public static void switchToAlertFrame(AppiumDriver driver) {
        testInfo("--- Switching to alert frame");
        long startTime = System.currentTimeMillis();
        long waitTime = TestData.MEDIUM_WAIT * 1000;
        boolean condition = false;
        while (!condition && (System.currentTimeMillis() - startTime) < waitTime) {
            try {
                driver.switchTo().alert();
                condition = true;
            } catch (NoAlertPresentException e) {
                testInfo("Alert is not visible. Wait a little bit.");
                condition = false;
            }
        }
    }

}
