package com.projectName.appium.utils.commonMethods;

import com.projectName.appium.utils.enums.DateFormats;
import com.projectName.appium.utils.enums.Language;
import org.testng.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static Date parseDate(String stringToParse, DateFormats format) {
        Date date = null;
        try {
            date = new SimpleDateFormat(format.getValue()).parse(stringToParse);
        } catch (ParseException e) {
            Assert.fail("Parse exception for format " + format + e.toString());
        }
        return date;
    }


    public static String changeDateFormat(Date date, DateFormats newFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(newFormat.getValue());
        return sdf.format(date);
    }

    public static String changeDateFormat(Date date, DateFormats newFormat, Language language) {
        SimpleDateFormat sdf = new SimpleDateFormat(newFormat.getValue(), language.getValue());
        return sdf.format(date);
    }

    public static String getCurrentDateInFormat(DateFormats format, Language language) {
        SimpleDateFormat formatter = new SimpleDateFormat(format.getValue(), language.getValue());
        Date date = new Date();
        return formatter.format(date);
    }

    public static String getCurrentDateInFormat(DateFormats format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format.getValue());
        Date date = new Date();
        return formatter.format(date);
    }

}
