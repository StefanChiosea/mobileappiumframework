package com.projectName.appium.utils.commonMethods;

import org.apache.commons.text.WordUtils;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.join;

/**
 * Created by azaharia on 26.06.2018.
 */
public class StringHelper {

    public static String splitByFourCharacters(String iban) {
        iban = iban.replaceAll("\\s+", "");
        return splitAtRegularIntervals(iban, 4);
    }

    public static String capitalizeFirstLetter(String string) {
        return WordUtils.capitalizeFully(string);
    }


    @SuppressWarnings("SameParameterValue")
    private static String splitAtRegularIntervals(String content, int chunk) {

        Pattern p = Pattern.compile("(.{" + chunk + "})");
        Matcher m = p.matcher(content);
        List<String> matches = new ArrayList<>();
        while (m.find()) {
            matches.add(m.group());
        }

        if (matches.size() * chunk < content.length()) {
            matches.add(content.substring(matches.size() * chunk));
        }

        return join(" ", matches);
    }

    public static String covertToUTF8(String input) {
        return String.valueOf(StandardCharsets.UTF_8.encode(input));
    }

}
