package com.projectName.appium.utils.commonMethods;

import com.google.gson.*;
import com.projectName.appium.pages.BasePage;
import com.projectName.appium.utils.Log4Test;
import com.projectName.appium.utils.enums.EndPoint;
import io.appium.java_client.AppiumDriver;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ExtractableResponse;
import io.restassured.specification.RequestSpecification;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;

public class CommonRestTask extends BasePage {

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private static int responseStatusCode;

    public CommonRestTask(AppiumDriver driver) {
        super(driver);
    }

    public static int getResponseStatusCode() {
        return responseStatusCode;
    }

    public static List<String> getListFromWSResponseString(EndPoint endPoint, Map<String, String> cookies, String path) {

        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()

                    .when()
                    .get(endPoint.getValue())

                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        JsonPath results = JsonPath.from(endPointResult.body().asString());
        return results.getList(path);
    }

    public static String getAccountProductName(JsonObject object) {

        Log4Test.info("Getting account : ");
        String value = "defaultValue";
        try {
            Log4Test.info("- getting account productName");
            value = object.get("productName").getAsString();
            Log4Test.info("productName : " + value);
        } catch (NullPointerException e2) {
            Log4Test.trace("" + e2);
            Log4Test.info("- productName field not found");
            try {
                Log4Test.info("- getting account id");
                value = object.get("id").getAsString();
                Log4Test.info("ID : " + value);
            } catch (NullPointerException e3) {
                Log4Test.trace("" + e3);
                testFailMessage("Account Type not found");
            }
        }
        return value;
    }

    public static String getAccountId(JsonObject object) {
        String value = null;
        try {
            value = object.get("id").getAsString();
        } catch (NullPointerException e3) {
            Log4Test.trace("" + e3);
            testFailMessage("Account Type not found");
        }
        return value;
    }

    public static JsonArray getJsonArrayFromEndpoint(EndPoint endPoint, Map<String, String> cookies) {

        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()
                    .when()
                    .get(endPoint.getValue())
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }

        String response = endPointResult.asString();

        return (JsonArray) new JsonParser().parse(response);
    }

    public static void deleteFromEndPoint(EndPoint endPoint, String pathParam, Map<String, String> cookies) {
        ExtractableResponse endPointResult;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()
                    .when()
                    .delete(endPoint.getValue() + pathParam)
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + pathParam + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + pathParam + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                case 400:
                    if (returnResponseBodyAsJson(endPointResult.body().asString()).contains("Template not found")) {
                        testWarnMessage("Template name not found : " + pathParam);
                    } else {
                        testFailMessage(endPoint.getValue() + pathParam + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    }
                    break breakAll;
                default:
                    testError(endPoint.getValue() + pathParam + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + pathParam + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + pathParam + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
    }

    public static JsonArray getJsonArrayFromEndpoint(EndPoint endPoint, String param, Map<String, String> cookies) {

        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()
                    .when()
                    .get(endPoint.getValue(param))
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        String response = endPointResult.asString();
        return (JsonArray) new JsonParser().parse(response);
    }

    public static JsonObject getJsonObjectFromEndpoint(EndPoint endPoint, String param, Map<String, String> cookies) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()
                    .when()
                    .get(endPoint.getValue(param))
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        String responseAsString = endPointResult.asString();
        return (JsonObject) new JsonParser().parse(responseAsString);
    }

    public static JsonObject getJsonObjectFromEndpoint(EndPoint endPoint, Map<String, String> cookie, String body) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookie)
                    .body(body)
                    .log().uri()
                    .log().body()
                    .log().method()
                    .when()
                    .post(endPoint.getValue())
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()) + " ||| request body is: " + body);
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testError(endPoint.getValue() + " request body body : " + body);
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()) + " ||| request body is: " + body);
                    break breakAll;
            }
        }
        String responseAsString = endPointResult.asString();
        return (JsonObject) new JsonParser().parse(responseAsString);
    }

    public static JsonArray postJsonArrayFromEndpoint(EndPoint endPoint, Map<String, String> cookie, String body) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookie)
                    .body(body)
                    .log().uri()
                    .log().body()
                    .log().method()
                    .when()
                    .post(endPoint.getValue())
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testError(endPoint.getValue() + " request body body : " + body);
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        String responseAsString = endPointResult.asString();
        return (JsonArray) new JsonParser().parse(responseAsString);
    }

    public static JsonObject postJsonObjectFromEndpoint(EndPoint endPoint, Map<String, String> cookie, String body) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookie)
                    .body(body)
                    .log().uri()
                    .log().body()
                    .log().method()
                    .when()
                    .post(endPoint.getValue())
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()) + " ||| request body is: " + body);
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testError(endPoint.getValue() + " request body body : " + body);
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()) + " ||| request body is: " + body);
                    break breakAll;
            }
        }
        String responseAsString = endPointResult.asString();
        return (JsonObject) new JsonParser().parse(responseAsString);
    }

    public static JsonObject getJsonObjectFromEndpoint(EndPoint endPoint, Map<String, String> cookies) {

        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()
                    .when()
                    .get(endPoint.getValue())
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        String response = endPointResult.asString();
        return (JsonObject) new JsonParser().parse(response);
    }

    public static void postJsonObj(EndPoint endPoint, Map<String, String> cookies, JsonObject obj) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .body(obj.toString())
                    .log().uri()
                    .log().body()
                    .log().method()
                    .when()
                    .post(endPoint.getValue())
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testError(endPoint.getValue() + " request body body : " + obj.toString());
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        Log4Test.info("Post Account List Order response : " + endPointResult.asString());
    }

    public static ExtractableResponse postStringBody(String endPoint, Map<String, String> cookies, String stringBody) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .body(stringBody)
                    .log().uri()
                    .log().body()
                    .log().method()
                    .when()
                    .post(endPoint)
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()) + " ||| request body is: " + stringBody);
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint + " status : " + endPointResult.statusCode());
                    testError(endPoint + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testError(endPoint + " request body body : " + stringBody);
                    testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()) + " ||| request body is: " + stringBody);
                    break breakAll;
            }
        }
        testDebugMessage("Post response : " + endPointResult.asString());
        return endPointResult;
    }

    public static ExtractableResponse postStringBodyMultiPart(String endPoint, Map<String, String> cookies, String key, String content) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "multipart/form-data")
                    
                    .cookies(cookies)
                    .formParam(key, content)
                    .multiPart(key, content, "multipart/form-data")
                    .log().uri()
                    .log().params()
                    .log().method()
                    .when()
                    .post(endPoint)
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint + " status : " + endPointResult.statusCode());
                    testError(endPoint + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testError(endPoint + " request body body : " + key + " : " + content);
                    testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        //testDebugMessage("Post response : " + endPointResult.asString());
        return endPointResult;
    }

    public static String getSingleElementFromResponseAsString(EndPoint endPoint, Map<String, String> cookies, String path) {

        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()

                    .when()
                    .get(endPoint.getValue())

                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        JsonPath results = JsonPath.from(endPointResult.body().asString());
        return results.get(path).toString();
    }

    private static RequestSpecification getRequestSpecifications() {

        RequestSpecification RequestSpecification;

        RequestSpecification = given().
                contentType(ContentType.JSON);

        return RequestSpecification;
    }

    public static JsonPath getJsonPathFromEndpoint(EndPoint endPoint) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            RequestSpecification requestSpecification = getRequestSpecifications();
            endPointResult = given()
                    .spec(requestSpecification)
                    .log().uri()
                    .log().method()
                    .get(endPoint.getValue())
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return JsonPath.from(endPointResult.body().asString());
    }


    public static ExtractableResponse getResponseFromEndpoint(String endPoint) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            RequestSpecification requestSpecification = getRequestSpecifications();
            endPointResult = given()
                    .spec(requestSpecification)
                    .log().uri()
                    .log().method()
                    .get(endPoint)
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint + " status : " + endPointResult.statusCode());
                    testError(endPoint + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return endPointResult;
    }

    public static ExtractableResponse getResponseFromEndpoint(String endPoint, Map<String, String> cookie) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-type", "application/json")
                    
                    .cookies(cookie)
                    .log().uri()
                    .log().method()
                    .get(endPoint)
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint + " status : " + endPointResult.statusCode());
                    testError(endPoint + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return endPointResult;
    }

    public static JsonPath getJsonPathFromEndpoint(EndPoint endPoint, String id, Map<String, String> cookie) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-type", "application/json")
                    
                    .cookies(cookie)
                    .log().uri()
                    .log().method()
                    .when()
                    .get(endPoint.getValue() + id)
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return JsonPath.from(endPointResult.body().asString());
    }

    public static JsonPath postJsonPathFromEndpoint(EndPoint endPoint, Map<String, String> cookie, String postBody) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-type", "application/json")
                    
                    .cookies(cookie)
                    .body(postBody)
                    .log().uri()
                    .log().body()
                    .log().method()
                    .when()
                    .post(endPoint.getValue())
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()) + " ||| request body is: " + postBody);
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()) + " ||| request body is: " + postBody);
                    break breakAll;
            }
        }
        return JsonPath.from(endPointResult.body().asString());
    }

    public static JsonPath getJsonPathFromEndpoint(String endPoint) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            RequestSpecification requestSpecification = getRequestSpecifications();
            endPointResult = given()
                    .spec(requestSpecification)
                    .log().uri()
                    .log().method()
                    .get(endPoint)
                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint + " status : " + endPointResult.statusCode());
                    testError(endPoint + " body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return JsonPath.from(endPointResult.body().asString());
    }

    public static JsonPath getJsonPathFromWSResponse(EndPoint endPoint, Map<String, String> cookies) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()

                    .when()
                    .get(endPoint.getValue())

                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return JsonPath.from(endPointResult.body().asString());
    }

    public static JsonPath getJsonPathFromWSResponse(EndPoint endPoint) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .log().uri()
                    .log().method()

                    .when()
                    .get(endPoint.getValue())

                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return JsonPath.from(endPointResult.body().asString());
    }

    public static JsonPath getJsonPathFromWSResponse(EndPoint endPoint, String param, Map<String, String> cookies) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()

                    .when()
                    .get(endPoint.getValue(param))

                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return JsonPath.from(endPointResult.body().asString());
    }


    public static ExtractableResponse wsGETResponse(EndPoint endPoint, Map<String, String> cookies) {

        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    //.log().all()
                    .log().uri()
                    //.log().body()
                    //.log().parameters()
                    .log().method()

                    .when()
                    .get(endPoint.getValue())

                    .then()
                    .log().status()
                    //.log().all()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return endPointResult;
    }


    public static ExtractableResponse wsGETResponse(EndPoint endPoint) {
        String endpointToGet;
        if (EndPoint.APPIUM_SESSION == endPoint) {
            endpointToGet = endPoint.getValueNonDefaultBaseURI();
        } else {
            endpointToGet = endPoint.getValue();
        }

        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {
            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    //.log().all()
                    .log().uri()
                    //.log().body()
                    //.log().parameters()
                    .log().method()

                    .when()
                    .get(endpointToGet)

                    .then()
                    .log().status()
                    //.log().all()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endPoint.getValue() + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endPoint.getValue() + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endPoint.getValue() + " status : " + endPointResult.statusCode());
                    testError(endPoint.getValue() + " response body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endPoint.getValue() + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return endPointResult;
    }

    public static String getStringFromEndPoint(EndPoint endpoint, String params, Map<String, String> cookies) {

        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {

            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()
                    .when()
                    .get(endpoint.getValue(params))

                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endpoint.getValue(params) + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endpoint.getValue(params) + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endpoint.getValue(params) + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endpoint.getValue(params) + " status : " + endPointResult.statusCode());
                    testError(endpoint.getValue(params) + " body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endpoint.getValue(params) + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return endPointResult.asString();
    }

    public static String getStringFromEndPoint(EndPoint endpoint, String stringHeader, String params, Map<String, String> cookies) {
        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {

            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "application/json")
                    
                    .header("Accept", "text/plain")
                    .cookies(cookies)
                    .log().uri()
                    .log().method()
                    .when()
                    .get(endpoint.getValue(params))

                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endpoint.getValue(params) + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endpoint.getValue(params) + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endpoint.getValue(params) + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endpoint.getValue(params) + " status : " + endPointResult.statusCode());
                    testError(endpoint.getValue(params) + " body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endpoint.getValue(params) + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return endPointResult.asString();
    }

    public static String getStringPlainTextFromEndPoint(EndPoint endpoint, String params, Map<String, String> cookies) {

        ExtractableResponse endPointResult = null;
        breakAll:
        for (int i = 0; i < 3; i++) {

            endPointResult = given()
                    .urlEncodingEnabled(false)
                    .header("Content-Type", "text/plain;charset=UTF-8")
                    
                    .cookies(cookies)
                    .log().uri()
                    .log().method()
                    .when()
                    .get(endpoint.getValue(params))

                    .then()
                    .log().status()
                    .extract();

            switch (endPointResult.statusCode()) {
                case 502:
                    testInfo(endpoint.getValue(params) + " status is 502! retrying!");
                    //noinspection ConstantConditions
                    if (i == 3) {
                        testError(endpoint.getValue(params) + " status was 502 3 times in row! Failing the test!");
                        testFailMessage(endpoint.getValue(params) + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                        break breakAll;
                    }
                    break;
                case 200:
                    break breakAll;
                default:
                    testError(endpoint.getValue(params) + " status : " + endPointResult.statusCode());
                    testError(endpoint.getValue(params) + " body : " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    testFailMessage(endpoint.getValue(params) + " not successful !!! " + endPointResult.statusCode() + " " + returnResponseBodyAsJson(endPointResult.body().asString()));
                    break breakAll;
            }
        }
        return endPointResult.asString();
    }

    public static String returnResponseBodyAsJson(String responseBody) {
        String value;
        if (responseBody.contains("<html><head>")) {
            if (!responseBody.contains("<style type=\"text/css\">")) {
                value = responseBody;
            } else {
                value = "Response contains css elements. Logs will not be displayed!";
            }
        } else {
            try {
                JsonElement jsonElement = new JsonParser().parse(responseBody
                        .replace("\\&quot;", "\"")
                        .replace("&quot;", "\"")
                        .replace("&quot", "\"")
                        .replace("\\\"", "\"")
                        .replace("\\n    ", "")
                        .replace("\\n", "")
                        .replace("\\u003d", "=")
                        .replace("\\u0026", "&")
                        .replace("\\u003c", "<")
                        .replace("\\u003e", ">")
                        .replace("\\u0027", "'"));
                value = gson.toJson(jsonElement);
            } catch (JsonSyntaxException jse) {
                Log4Test.trace("" + jse);
                value = responseBody;
            }
        }
        return value;
    }

    public static String jsonPathStringBuilder(ExtractableResponse wsResponse, String jsonPath) {
        try {
            //testDebugMessage("+++++++++++++++" + wsResponse.body().toString());
            return JsonPath.from(wsResponse.body().asString()).getString(jsonPath).replace("[", "").replace("]", "");
        } catch (NullPointerException npe) {
            Log4Test.trace("" + npe);
            try {
                return JsonPath.from(wsResponse.body().toString()).getString(jsonPath).replace("[", "").replace("]", "");
            } catch (NullPointerException npe2) {
                testError("NPE error for: " + jsonPath + " on ws response: " + wsResponse.body());
                throw npe;
            }
        }
    }


    public static JsonPath returnJsonPathWithQuotesForBlanaceFromJsonArray(JsonArray jsonArray, String arrayName) {
        List<String> filteredList = new ArrayList<>();

        testInfo("Adding quotes \"\" to JsonArray: " + arrayName);

        int jsonArraySize = jsonArray.size();
        for (int z = 0; z < jsonArraySize; z++) {
            String initialString = gson.toJson(jsonArray.get(z));

            final Pattern balancePattern = Pattern.compile("\"balance\": -?(\\d+).(\\d+),");
            final Pattern amountPattern = Pattern.compile("-?(\\d+).(\\d+)");
            String balanceWithAmount = "balanceWithAmount";
            Matcher matcher1 = balancePattern.matcher(initialString);
            if (matcher1.find()) {
                balanceWithAmount = matcher1.group(0);
                //testDebugMessage(balanceWithAmount);
            } else {
                testDebugMessage(initialString);
                testFailMessage("no match found for this regex: \"balance\": -?(\\d+).(\\d+),");
            }
            Matcher matcher2 = amountPattern.matcher(balanceWithAmount);
            String amount = "amount";
            if (matcher2.find()) {
                amount = matcher2.group(0);
            } else {
                testDebugMessage(balanceWithAmount);
                testFailMessage("no match found for this regex: -?(\\d+).(\\d+)");
            }
            initialString = initialString
                    .replace(": " + amount, ": \"" + amount + "\"")
                    .replace(":" + amount, ":\"" + amount + "\"")
                    .replace("\"\"", "\"");
            filteredList.add(initialString);
        }

        String returnGsonString = gson.toJson(filteredList.toString())
                .replace("\"[", "[")
                .replace("]\"", "]")
                .replace("\\n", "")
                .replace("\\n  ", "")
                .replace("        ", "")
                .replace("      ", "")
                .replace("    ", "")
                .replace("  ", "")
                .replace("\\\"", "\"");

        testInfo("Completed adding quotes \"\" to JsonArray: " + arrayName + ". Will return JsonPath now.");
        return JsonPath.from(returnGsonString);
    }

    public static JsonPath returnJsonPathWithQuotesForBalanceFromString(String keyValue, String stringPathValue, String arrayName) {
        List<String> filteredList = new ArrayList<>();

        testInfo("Adding quotes \"\" to JsonArray: " + arrayName);

        String initialString = stringPathValue;

        final Pattern balancePattern = Pattern.compile("\"" + keyValue + "\": -?(\\d+).(\\d+),");
        final Pattern amountPattern = Pattern.compile("-?(\\d+).(\\d+)");
        String balanceWithAmount = "balanceWithAmount";
        Matcher matcher1 = balancePattern.matcher(initialString);
        if (matcher1.find()) {
            balanceWithAmount = matcher1.group(0);
            //testDebugMessage(balanceWithAmount);
        } else {
            testDebugMessage(initialString);
            testFailMessage("no match found for this regex: \"" + keyValue + "\": -?(\\d+).(\\d+),");
        }
        Matcher matcher2 = amountPattern.matcher(balanceWithAmount);
        String amount = "amount";
        if (matcher2.find()) {
            amount = matcher2.group(0);
        } else {
            testDebugMessage(balanceWithAmount);
            testFailMessage("no match found for this regex: -?(\\d+).(\\d+)");
        }
        initialString = initialString
                .replace(": " + amount, ": \"" + amount + "\"")
                .replace(":" + amount, ":\"" + amount + "\"")
                .replace("\"\"", "\"");
        filteredList.add(initialString);


        String returnGsonString = gson.toJson(filteredList.toString())
                .replace("\"[", "[")
                .replace("]\"", "]")
                .replace("\\n", "")
                .replace("\\n  ", "")
                .replace("        ", "")
                .replace("      ", "")
                .replace("    ", "")
                .replace("  ", "")
                .replace("\\\"", "\"");

//        testInfo("Completed adding quotes \"\" to JsonArray: " + arrayName + ". Will return JsonPath now.");
        return JsonPath.from(returnGsonString);
    }


}
