package com.projectName.appium.utils.commonMethods;

import java.lang.reflect.Array;


public class ArrayHelper {

    public static Object addElementInFrontOfArray(Object array, Object element) {
        int newArraySize = Array.getLength(array) + 1;
        Object newArray = Array.newInstance(element.getClass(), newArraySize);
        //Add first element
        Array.set(newArray, 0, element);
        for (int i = 1; i < newArraySize; i++) {
            Array.set(newArray, i, Array.get(array, i - 1));
        }
        return newArray;
    }

}
