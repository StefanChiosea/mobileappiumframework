package com.projectName.appium.utils.commonMethods;

import com.projectName.appium.utils.enums.Language;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;


public class NumberFormatHelper {


    public static String formatThousandSeparatorAndTwoDigits(String value, Language language) {
        Double doubleValue = Double.parseDouble(value);

        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(language.getValue());
        DecimalFormat df = new DecimalFormat("#,##0.00", formatSymbols);
        df.setGroupingSize(3);
        df.setRoundingMode(RoundingMode.FLOOR);
        return df.format(doubleValue);

    }

    public static String formatThousandSeparatorAndTwoDigits(String value, Language language, RoundingMode roundingMode) {
        Double doubleValue = Double.parseDouble(value);

        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(language.getValue());
        DecimalFormat df = new DecimalFormat("#,##0.00", formatSymbols);
        df.setGroupingSize(3);
        df.setRoundingMode(roundingMode);
        return df.format(doubleValue);

    }

    public static String formatThousandSeparatorAndTwoDigitsIfNotZero(String value, Language language) {
        // if digits end in 0 it will be removed
        Double sumFromWs = Double.parseDouble(value);

        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(language.getValue());
        DecimalFormat df = new DecimalFormat("#,##0.##", formatSymbols);
        df.setGroupingSize(3);
        return df.format(sumFromWs);
    }

    public static String formatThousandSeparatorAndTwoForIfNotZero(String value, Language language) {
        // if digits end in 0 it will be removed
        Double sumFromWs = Double.parseDouble(value);

        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(language.getValue());
        DecimalFormat df = new DecimalFormat("#,##0.####", formatSymbols);
        df.setGroupingSize(3);
        return df.format(sumFromWs);
    }

    public static String formatThousandSeparator(String value, Language language) {
        Double sumFromWs = Double.parseDouble(value);
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(language.getValue());
        DecimalFormat df = new DecimalFormat("#.0000", formatSymbols);
        return df.format(sumFromWs);
    }
}
