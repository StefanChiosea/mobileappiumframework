package com.projectName.appium.utils;

/*import org.apache.log4j.Logger;
import org.apache.logging.log4j.Level;*/

import com.epam.reportportal.service.ReportPortal;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Calendar;

/**
 * Created by chv on 07.02.2017.
 * <p>
 * Log4Test configuration class
 */
@SuppressWarnings("ALL")
public class Log4Test {
    //private static final Logger LOGGER = Logger.getLogger(Log4Test.class);
    public static final Logger LOGGER = LogManager.getLogger(Log4Test.class);

    // ****************** Default message content ********************
/*    private static final String INFO_LOG = "INFO: \"%s\"";
    private static final String ERROR_LOG = "ERROR: \"%s\" !";
    private static final String TEST_LOG = "TEST: \"%s\"";*/

    private static final String INFO_LOG = " \"%s\"";
    private static final String ERROR_LOG = "---ERROR--- \"%s\" !";
    private static final String TEST_LOG = " \"%s\"";
    private static final String WARN_LOG = "---WARN--- \"%s\"";
    private static final String DEBUG_LOG = " \"%s\"";
    private static final String TRACE_LOG = " \"%s\"";
    private static final String FATAL_LOG = "---FATAL--- \"%s\"";

    private Log4Test() {
    }

    public static String error(String message) {
        LOGGER.error(String.format(ERROR_LOG, message));
        Reporter.log(String.format(ERROR_LOG, message));
        return String.format(ERROR_LOG, message);
    }

    public static String pass(String message) {
        LOGGER.log(Level.forName("PASS", 350), (String.format(TEST_LOG, message)));
        Reporter.log(String.format(TEST_LOG, message));
        return String.format(TEST_LOG, message);
    }

    public static void info(String message) {
        LOGGER.info(String.format(INFO_LOG, message));
        Reporter.log(String.format(INFO_LOG, message));
        String.format(INFO_LOG, message);
    }

    /*public static void test(String message) {
        LOGGER.info(String.format(TEST_LOG, message));
        Reporter.log(String.format(TEST_LOG, message));
        String.format(TEST_LOG, message);
    }*/

    public static String test(String message) {
        LOGGER.log(Level.forName("TEST", 350), (String.format(TEST_LOG, message)));
        Reporter.log(String.format(TEST_LOG, message));
        return String.format(TEST_LOG, message);
    }

    public static String warn(String message) {
        LOGGER.warn(String.format(WARN_LOG, message));
        Reporter.log(String.format(WARN_LOG, message));
        return String.format(WARN_LOG, message);
    }

    public static String debug(String message) {
        LOGGER.debug(String.format(DEBUG_LOG, message));
        Reporter.log(String.format(DEBUG_LOG, message));
        return String.format(DEBUG_LOG, message);
    }

    public static String trace(String message) {
        LOGGER.trace(String.format(TRACE_LOG, message));
        Reporter.log(String.format(TRACE_LOG, message));
        return String.format(TRACE_LOG, message);
    }


    public static String fatal(String message) {
        LOGGER.fatal(String.format(FATAL_LOG, message));
        Reporter.log(String.format(FATAL_LOG, message));
        return String.format(FATAL_LOG, message);
    }

    public static void uploadSS(File file, String message) {
        ReportPortal.emitLog(message, "ERROR", Calendar.getInstance().getTime(), file);
    }

    @Test
    public void colorsTest() {
        Log4Test.info("System out text");
        Log4Test.info("Info text");
        Log4Test.test("Test text");
        Log4Test.warn("Warn text");
        Log4Test.error("Error text");
        Log4Test.debug("Debug text");
        Log4Test.trace("Trace text");
        Log4Test.fatal("Fatal text");
    }
}
