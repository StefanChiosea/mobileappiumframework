package com.projectName.appium.utils.skipInterfaces;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import java.lang.reflect.Method;

import static com.projectName.appium.listeners.ExtentTestListener.testSkipMessage;

public class ConditionalSkipTestAnalyzer implements IInvokedMethodListener {

    public void beforeInvocation(IInvokedMethod invokedMethod, ITestResult result) {
        Method method = result.getMethod().getConstructorOrMethod().getMethod();

        if (method == null) {
            return;
        }

        if (method.isAnnotationPresent(Wip.class)) {
            testSkipMessage("Skipped because this is a wip test");
        }
    }

    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
    }
}