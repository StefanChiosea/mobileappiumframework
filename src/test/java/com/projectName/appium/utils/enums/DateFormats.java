package com.projectName.appium.utils.enums;

/**
 * Created by azaharia on 21.06.2018.
 */
public enum DateFormats {

    DATE_FORMAT_PATTERN_FROM_FX_WS("yyyy-MM-dd'T'HH:mm:ss'Z'"),
    DATE_FORMAT_PATTERN_FROM_FX_WS2("yyyy-MM-dd'T'HH:mm:ss"),
    DATE_FORMAT_PATTERN_FROM_INTEREST_RATES_WS("yyyy-MM-dd'T'HH:mm:ss"),
    DATE_FORMAT_PATTERN_CURRENT_DATE_WITH_TIME("yyyy-MM-dd'T'HH:mm:ss"),
    DATE_FORMAT_yyyy_MM_dd_DASH("yyyy-MM-dd"),
    DATE_FORMAT_yyyy_MM_dd_DASH_TIMESTAMP("yyyy-MM-dd'T'HH:mm:ss.SSSSSS"),
    DATE_FORMAT_MMMM_dd_YYYY_SPACE("MMMM dd YYYY"),
    DATE_FORMAT_MMMM_dd_yyyy_SPACE("MMMM dd yyyy"),
    DATE_FORMAT_MMMM_d_YYYY_SPACE("MMMM d YYYY"),
    DATE_FORMAT_MMMM_dd_YYYY_COMMA("MMMM dd, YYYY"),
    DATE_FORMAT_MMMM_d_YYYY_COMMA("MMMM d, YYYY"),
    DATE_FORMAT_BILL_PAYMENT_INVOICE_DATE("dd.MM.YYYY"),
    DATE_FORMAT_MMMM_d_yyyy_COMMA("MMMM d, yyyy"),
    DATE_FORMAT_d_MMMM_YYYY_COMMA("d MMMM, yyyy"),
    DATE_FORMAT_yyyy_MM_dd_SLASH("yyyy/MM/dd"),
    DATE_FORMAT_dd_MM_yyyy_SLASH("dd/MM/yyyy"),
    DATE_FORMAT_dd_MM_yyyy_HH_mm("dd.MM.yyyy HH:mm"),
    DATE_FORMAT_dd_MMM("dd MMM"),
    DATE_FORMAT_dd_MM("dd MM"),
    DATE_FORMAT_YYYY("YYYY"),
    DATE_FORMAT_YY("YY"),
    DATE_FORMAT_MMMM("MMMM"),
    DATE_FORMAT_MM("MM"),
    DATE_FORMAT_dd("dd"),
    DATE_FORMAT_dd_MMMM_yyyy_SPACE("dd MMMM yyyy"),
    DATE_FORMAT_dd_MMMM_yyyy_SPACE_WITH_TIME_RO("dd MMMM yyyy HH:mm"),
    DATE_FORMAT_d_MMMM_yyyy_COMA_SPACE_WITH_TIME_RO("dd MMMM yyyy',' HH:mm"),
    DATE_FORMAT_d_MMM_yyyy_SPACE_WITH_TIME_RO("dd MMM yyyy HH:mm"),
    DATE_FORMAT_d_MMM_DOT_yyyy_SPACE_WITH_TIME_RO("dd MMM'.' yyyy HH:mm"),
    DATE_FORMAT_d_MMMM_DOT_yyyy_SPACE_WITH_TIME_RO("dd MMMM'.' yyyy HH:mm"),
    DATE_FORMAT_MMMM_RO("MMMM"),
    DATE_FORMAT_dd_MMMM_yyyy_SPACE_WITH_TIME_IOS_RO("d MMMM yyyy, HH:mm"),
    DATE_FORMAT_dd_MMMM_yyyy_COMMA_WITH_TIME_EN("MMMM dd, yyyy HH:mm"),
    DATE_FORMAT_dd_MMMM_yyyy_COMMA_WITH_TIME_IOS_EN("MMMM d, yyyy 'at' H:mm a"),
    DATE_FORMAT_dd_MMMM_yyyy_COMMA_WITH_TIME_IOS_EN_2("MMMM d, yyyy 'at' h:mm a"),
    DATE_FORMAT_MMMM_d_SPACE("MMMM d"),
    DATE_FORMAT_d_MMMM_SPACE("d MMMM"),
    DATE_FORMAT_d_mmm_SPACE("d mmm"),
    DATE_FORMAT_d_MMM_SPACE("d MMMM"),
    DATE_FORMAT_d_MMMM_yyyy_SPACE("d MMMM yyyy"),
    DATE_FORMAT_MMMM_dd_yyyy_SPACE_IOS("MMMM dd yyyy"),
    DATE_FORMAT_dd_mmmm_yyyy_SPACE("dd mmmm yyyy"),
    DATE_FORMAT_dd_MMM_yyyy_SPACE("dd MMM yyyy"),
    DATE_FORMAT_dd_mmm_yyyy_SPACE("dd mmm yyyy"),
    DATE_FORMAT_yyyy_mm_dd_SPACE_TIME("yyyy-MM-dd HH:mm:ss"),
    DATE_FORMAT_TIME("HHmmss"),
    DATE_FORMAT_TIME_SPLIT("HH:mm"),
    DATE_FORMAT_DD_MMM_yyyy_SPACE("DD MMM yyyy"),
    DATE_FORMAT_dd_MM_yyyy_DOT("dd.MM.yyyy"),
    DATE_FORMAT_dd_MM_yyyy_DASH("dd-MM-yyyy"),
    DATE_FORMAT_MMMM_yyyy_SPACE("MMMM yyyy");


    private final String value;

    DateFormats(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
