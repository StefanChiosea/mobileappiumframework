package com.projectName.appium.utils.enums;


/**
 * Created by azaharia on 24.04.2018.
 */
public enum EndPoint {

    APPIUM_SESSION(System.getProperty("appiumServerLocation") + "/sessions");

    public static final String baseURI = "";
    private final String value;

    EndPoint(String value) {
        this.value = value;
    }

    public String getValue() {
        return baseURI + value;
    }

    public String getValueNonDefaultBaseURI() {
        return value;
    }

    public String getValue(String param) {
        return baseURI + value + param;
    }

}
