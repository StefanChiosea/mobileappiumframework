package com.projectName.appium.utils.enums;


import java.util.Locale;


public enum Language {
    EN(Locale.ENGLISH),
    RO(new Locale("ro", "RO"));

    private final Locale value;

    Language(Locale value) {
        this.value = value;
    }

    public Locale getValue() {
        return value;
    }
}
