package com.projectName.appium;

import com.projectName.appium.config.AppiumFactory;
import com.projectName.appium.listeners.BaseTestNGListenerWithParameter;
import com.projectName.appium.listeners.ExtentTestListener;
import com.projectName.appium.utils.AppiumServerJava;
import com.projectName.appium.utils.Log4Test;
import com.projectName.appium.utils.enums.EndPoint;
import com.projectName.appium.utils.skipInterfaces.ConditionalSkipTestAnalyzer;
import io.appium.java_client.AppiumDriver;
import io.restassured.RestAssured;
import org.openqa.selenium.NoSuchSessionException;
import org.testng.Assert;
import org.testng.ITest;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Listeners({ExtentTestListener.class, ConditionalSkipTestAnalyzer.class, BaseTestNGListenerWithParameter.class})
public class AppiumBase implements ITest {

    private static final List<AppiumFactory> webDriverThreadPool = Collections.synchronizedList(new ArrayList<>());
    private static ThreadLocal<AppiumFactory> appiumFactory;
    private static ThreadLocal<String> testName = new ThreadLocal<>();

    public static AppiumDriver getDriver() {
        return appiumFactory.get().getDriver();
    }

    @AfterMethod(alwaysRun = true)
    public static void closeDriverObjects() {
        Log4Test.info("Trying to close the app");
        try {
            getDriver().closeApp();
            for (AppiumFactory appiumFactory : webDriverThreadPool) {
                appiumFactory.quitDriver();
                testName.set("instantiateDriverObject");
            }
        } catch (NoSuchSessionException nsse) {
            Log4Test.trace("" + nsse);
        }
    }


    @BeforeMethod(alwaysRun = true)
    public static void instantiateDriverObject() {
        // smart token init
        verifySmartTokenSetup();
        initializeDriverSetup();
        boolean loginWithRegisterDevice = Boolean.parseBoolean(System.getProperty("registerDeviceLoginBoolean"));

        if (loginWithRegisterDevice) {
            closeDriverObjects();
            initializeDriverSetup();
        }
        if (!loginWithRegisterDevice) {
            getDriver().resetApp();
        }

        Log4Test.info("                                                             ");
        Log4Test.info("------------------- START TEST");
        RestAssured.baseURI = EndPoint.baseURI;
        RestAssured.useRelaxedHTTPSValidation();

        testName.set("setTestName");
    }

    private static void verifySmartTokenSetup() {
        boolean smartTokenEnvPropsFlag = Boolean.parseBoolean(System.getProperty("smartTokenLoginBoolean"));

        if (smartTokenEnvPropsFlag) {
            String device = System.getProperty("driverType");
            switch (device) {
                case "ANDROID":
                    System.setProperty("driverType", "ANDROID_SMART_TOKEN");
                    initializeDriverSetup();
                    getDriver().launchApp();
                    closeDriverObjects();
                    System.setProperty("driverType", "ANDROID");
                    break;
                case "IOS":
                    System.setProperty("driverType", "IOS_SMART_TOKEN");
                    initializeDriverSetup();
                    getDriver().resetApp();
                    closeDriverObjects();
                    System.setProperty("driverType", "IOS");
                    break;
                default:
                    Assert.fail("Wrong device param");
            }
        }
    }

    private static void initializeDriverSetup() {
        appiumFactory = ThreadLocal.withInitial(() -> {
            AppiumFactory appiumFactory = new AppiumFactory();
            webDriverThreadPool.add(appiumFactory);
            return appiumFactory;
        });
    }

    @AfterSuite(alwaysRun = true)
    public static void closeAppiumServer() {
        Log4Test.info("Trying to close Appium server for device: " + System.getProperty("deviceRunOption"));
        AppiumServerJava.stopServer();
    }

    @BeforeSuite(alwaysRun = true)
    public void startAppium() {
        AppiumServerJava.startServer();
    }

    protected void setCloseDriverObjectsMethodName() {
        testName.set("closeDriverObjects");
    }

    @BeforeMethod(alwaysRun = true)
    public void setTestName(Method method, Object[] testData, ITestContext ctx) {
        Test test = method.getAnnotation(Test.class);
        String testDescription = test.description();
        if (testData.length > 0) {
            if (!"".equals(testDescription)) {
                testName.set(testDescription + "_" + Arrays.toString(testData).replace(",", "_"));
            } else {
                testName.set(method.getName() + "_" + Arrays.toString(testData).replace(",", "_"));
            }
            ctx.setAttribute("testName", testName.get());
        } else {
            if (!"".equals(testDescription)) {
                testName.set(testDescription);
            } else {
                testName.set(method.getName());
            }
            ctx.setAttribute("testName", testName.get());
        }
    }

    @Override
    public String getTestName() {
        return testName.get();
    }

}
